/**
 * @author tla
 * @see {@link https://framagit.org/tla}
 *
 * JS.Kitten
 * @see {@link https://framagit.org/tla/JS.Kitten}
 *
 * @license MIT
 * @see {@link https://framagit.org/tla/JS.Kitten/blob/master/LICENSE}
 *
 * @use js-tools-kit
 * @see {@link https://framagit.org/tla/js-tools-kit}
 *
 * @use targetJS
 * @see {@link https://framagit.org/tla/targetJS/targetJS}
 *
 * @use event-handler
 * @see {@link https://framagit.org/tla/event-handler}
 *
 * @use css3-animation
 * @see {@link https://framagit.org/tla/css3-animation}
 *
 * @use vanillajax
 * @see {@link https://framagit.org/tla/vanillajax}
 * */
( ( function ( build ) {
    
    var global ,
        debug = true;
    
    try {
        global = Function( 'return this' )() || ( 42, eval )( 'this' );
    } catch ( e ) {
        global = window;
    }
    
    var origin = global.console ,
        console = {};
    
    [ 'log' , 'info' , 'error' , 'time' , 'timeEnd' ].forEach( function ( key ) {
        console[ key ] = function () {
            if ( debug ) {
                if ( arguments.length > 1 ) {
                    origin[ key ].apply( origin , Array.apply( null , arguments ) );
                }
                else {
                    origin[ key ].call( origin , arguments[ 0 ] );
                }
            }
        };
    } );
    
    build( console , global );
    
} )( ( function ( console , global ) {
    
    'use strict';
    
    console.time( 'Kitten build time' );
    
    var concat = Array.prototype.push ,
        hasProperty = Object.prototype.hasOwnProperty;
    
    /**
     * js-tools-kit
     * @license MIT
     * @see {@link https://framagit.org/tla/js-tools-kit}
     * */
    ( function () {
        
        function _defineProperty ( obj , key , prop ) {
            Object.defineProperty( obj , key , {
                configurable : false ,
                enumerable : true ,
                writable : false ,
                value : prop
            } );
        }
        
        /**
         * @readonly
         * @property {Window} global
         * */
        _defineProperty( global , 'global' , global );
        
        /**
         * @readonly
         * @function _defineProperty
         * @param {Object} obj
         * @param {string} key
         * @param {*} prop
         * */
        _defineProperty( global , '_defineProperty' , _defineProperty );
        
        /**
         * @readonly
         * @function isset
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isset' , function ( _ ) {
            return _ !== undefined;
        } );
        
        /**
         * @readonly
         * @function isnull
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnull' , function ( _ ) {
            return _ === null;
        } );
        
        /**
         * @readonly
         * @function exist
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'exist' , function ( _ ) {
            return isset( _ ) && !isnull( _ );
        } );
        
        /**
         * @readonly
         * @function isdate
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isdate' , function ( _ ) {
            return exist( _ ) && _ instanceof Date;
        } );
        
        /**
         * @readonly
         * @function isarray
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isarray' , function ( _ ) {
            return exist( _ ) && Array.isArray( _ );
        } );
        
        /**
         * @readonly
         * @function isobject
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isobject' , function ( _ ) {
            return exist( _ ) &&
                _ instanceof Object && _.toString() === '[object Object]' &&
                _.constructor.hasOwnProperty( 'defineProperty' );
        } );
        
        /**
         * @readonly
         * @function isstring
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isstring' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'string' || _ instanceof global.String );
        } );
        
        /**
         * @readonly
         * @function isfillstring
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfillstring' , function ( _ ) {
            return isstring( _ ) && !!_.trim();
        } );
        
        /**
         * @readonly
         * @function isboolean
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isboolean' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'boolean' || _ instanceof global.Boolean );
        } );
        
        /**
         * @readonly
         * @function isinteger
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isinteger' , function ( _ ) {
            return exist( _ ) && Number.isInteger( _ );
        } );
        
        /**
         * @readonly
         * @function isfloat
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfloat' , function ( _ ) {
            return exist( _ ) && typeof _ == 'number' && isFinite( _ );
        } );
        
        /**
         * @readonly
         * @function isfunction
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfunction' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'function' || _ instanceof global.Function );
        } );
        
        /**
         * @readonly
         * @function isevent
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isevent' , function ( _ ) {
            if ( exist( _ ) ) {
                return ( _ instanceof global.Event ) ||
                    ( exist( _.defaultEvent ) && _.defaultEvent instanceof global.Event ) ||
                    ( exist( _.originalEvent ) && _.originalEvent instanceof global.Event );
            }
            return false;
        } );
        
        /**
         * @readonly
         * @function isregexp
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isregexp' , function ( _ ) {
            return exist( _ ) && ( _ instanceof global.RegExp );
        } );
        
        /**
         * @readonly
         * @function isnodelist
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnodelist' , function ( _ ) {
            return exist( _ ) && (
                ( _ instanceof global.NodeList || _ instanceof global.HTMLCollection ) ||
                ( !isset( _.slice ) && isset( _.length ) && typeof _ === 'object' )
            );
        } );
        
        /**
         * @readonly
         * @function isdocument
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isdocument' , function ( _ ) {
            return exist( _ ) && exist( _.defaultView ) && _ instanceof _.defaultView.HTMLDocument;
        } );
        
        /**
         * @readonly
         * @function iswindow
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'iswindow' , function ( _ ) {
            return exist( _ ) && ( _ === global || ( exist( _.window ) &&
                exist( _.window.constructor ) && _ instanceof _.window.constructor ) );
        } );
        
        /**
         * @readonly
         * @function getdocument
         * @param {*} _
         * @return {?HTMLDocument}
         * */
        _defineProperty( global , 'getdocument' , function ( _ ) {
            var tmp;
            
            if ( exist( _ ) ) {
                
                if ( isdocument( _ ) ) {
                    return _;
                }
                
                if ( isdocument( tmp = _.ownerDocument ) ) {
                    return tmp;
                }
                
                if ( iswindow( _ ) ) {
                    return _.document;
                }
                
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function getwindow
         * @param {*} _
         * @return {!Window}
         * */
        _defineProperty( global , 'getwindow' , function ( _ ) {
            var tmp;
            
            if ( iswindow( _ ) ) {
                return _;
            }
            
            if ( tmp = getdocument( _ ) ) {
                return tmp.defaultView;
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function isnode
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnode' , function ( _ ) {
            var doc , tag;
            
            if ( ( doc = getdocument( _ ) ) && isstring( tag = _.tagName ) ) {
                return ( doc.createElement( tag ) instanceof _.constructor ||
                    getdocument( global ).createElement( tag ) instanceof _.constructor );
            }
            
            return false;
        } );
        
        /**
         * @readonly
         * @function isfragment
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfragment' , function ( _ ) {
            var win;
            
            return ( win = getwindow( _ ) ) &&
                ( _ instanceof win.DocumentFragment || _ instanceof global.DocumentFragment );
        } );
        
        /**
         * @readonly
         * @property {boolean} eventPassiveSupport
         * */
        ( function () {
            var tmp ,
                sup = false ,
                document = getdocument( global );
            
            if ( document ) {
                tmp = document.createDocumentFragment();
                
                var empty = function () {} , setting = {
                    get passive () {
                        sup = true;
                        return false;
                    }
                };
                
                tmp.addEventListener( 'click' , empty , setting );
            }
            
            _defineProperty( global , 'eventPassiveSupport' , sup );
        } )();
        
        /**
         * @readonly
         * @function domLoad
         * @param {function} callback
         * */
        _defineProperty( global , 'domLoad' , ( function () {
            var ready = false ,
                stackfunction = [] ,
                document = getdocument( global );
            
            function stack ( callback ) {
                if ( isfunction( callback ) ) {
                    if ( ready ) {
                        return callback.call( document );
                    }
                    
                    stackfunction.push( callback );
                }
            }
            
            function load () {
                if ( !ready ) {
                    ready = true;
                    
                    stackfunction.forEach( function ( callback ) {
                        callback.call( document );
                    } );
                    
                    stackfunction = null;
                    
                    if ( document ) {
                        document.removeEventListener( 'DOMContentLoaded' , load , true );
                        document.removeEventListener( 'DomContentLoaded' , load , true );
                        document.removeEventListener( 'load' , load , true );
                    }
                    
                    global.removeEventListener( 'load' , load , true );
                }
            }
            
            if ( document ) {
                document.addEventListener( 'DOMContentLoaded' , load , true );
                document.addEventListener( 'DomContentLoaded' , load , true );
                document.addEventListener( 'load' , load , true );
            }
            
            global.addEventListener( 'load' , load , true );
            
            return stack;
        } )() );
        
        /**
         * @readonly
         * @function matchesSelector
         * @param {HTMLElement|Element|Node} e - element
         * @param {string} s - css selector
         * @return {!boolean}
         * */
        _defineProperty( global , 'matchesSelector' , ( function () {
            var document = getdocument( global ) ,
                match = null ,
                tmp;
            
            if ( document ) {
                tmp = document.createElement( 'div' );
                
                match = tmp[ 'matches' ] && 'matches' ||
                    tmp[ 'matchesSelector' ] && 'matchesSelector' ||
                    tmp[ 'webkitMatchesSelector' ] && 'webkitMatchesSelector' ||
                    tmp[ 'mozMatchesSelector' ] && 'mozMatchesSelector' ||
                    tmp[ 'oMatchesSelector' ] && 'oMatchesSelector' ||
                    tmp[ 'msMatchesSelector' ] && 'msMatchesSelector' || null;
            }
            
            return function ( e , s ) {
                if ( !match || !e[ match ] ) {
                    return false;
                }
                
                return e[ match ]( s );
            };
        } )() );
        
        /**
         * @readonly
         * @function nl2br
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'nl2br' , function ( str ) {
            if ( isstring( str ) ) {
                return str
                    .replace( /\n/g , "<br/>" )
                    .replace( /\t/g , "&nbsp;&nbsp;&nbsp;&nbsp;" );
            }
            
            return '';
        } );
        
        /**
         * @readonly
         * @function inarray
         * @param {*} _in
         * @param {Array} array
         * @return {!boolean}
         * */
        _defineProperty( global , 'inarray' , function ( _in , array ) {
            return array.indexOf( _in ) >= 0;
        } );
        
        /**
         * @readonly
         * @function arrayunique
         * @param {Array} _
         * @return {!Array}
         * */
        _defineProperty( global , 'arrayunique' , function ( _ ) {
            for ( var n = [] , i = 0 , l = _.length ; i < l ; i++ ) {
                if ( !inarray( _[ i ] , n ) ) {
                    n.push( _[ i ] );
                }
            }
            return n;
        } );
        
        /**
         * @readonly
         * @function typedtoarray
         * @param {*} arg
         * @return {!Array}
         * */
        _defineProperty( global , 'typedtoarray' , function ( arg ) {
            return Array.apply( null , arg );
        } );
        
        /**
         * @readonly
         * @function fragmenttoarray
         * @param {DocumentFragment} frag
         * @return {!Array}
         * */
        _defineProperty( global , 'fragmenttoarray' , function ( frag ) {
            return typedtoarray( frag.children );
        } );
        
        /**
         * @readonly
         * @function getallchildren
         * @param {HTMLElement|Element|Node} element
         * @return {!NodeList}
         * */
        _defineProperty( global , 'getallchildren' , function ( element ) {
            return element.getElementsByTagName( '*' );
        } );
        
        /**
         * @readonly
         * @function emptyNode
         * @param {HTMLElement|Element|Node} element
         * @return {!(HTMLElement|Element|Node)}
         * */
        _defineProperty( global , 'emptyNode' , function ( element ) {
            var c;
            
            while ( c = element.firstChild ) {
                element.removeChild( c );
            }
            
            return element;
        } );
        
        /**
         * @readonly
         * @function closest
         * @param {HTMLElement|Element|Node} element
         * @param {string} selector
         * @return {?(HTMLElement|Element|Node)}
         * */
        _defineProperty( global , 'closest' , ( function () {
            var document = getdocument( global );
            
            if ( document && isfunction( document.createElement( 'div' ).closest ) ) {
                return function ( element , selector ) {
                    if ( !isnode( element ) ) {
                        return null;
                    }
                    
                    return element.closest( selector );
                };
            }
            
            return function ( element , selector ) {
                if ( !isnode( element ) ) {
                    return null;
                }
                
                if ( matchesSelector( element , selector ) ) {
                    return element;
                }
                
                while ( isnode( element = element.parentNode ) ) {
                    if ( matchesSelector( element , selector ) ) {
                        return element;
                    }
                }
                
                return null;
            };
        } )() );
        
        /**
         * @readonly
         * @function randomstring
         * @return {!string}
         * */
        _defineProperty( global , 'randomstring' , function () {
            return '_' + Math.random().toString( 30 ).substring( 2 );
        } );
        
        /**
         * @readonly
         * @function between
         * @param {number} min
         * @param {number} max
         * @return {!number}
         * */
        _defineProperty( global , 'between' , function ( min , max ) {
            return Math.floor( Math.random() * ( max - min + 1 ) + min );
        } );
        
        /**
         * @readonly
         * @function round
         * @param {number} int
         * @param {number} after
         * @return {!number}
         * */
        _defineProperty( global , 'round' , function ( int , after ) {
            return parseFloat( int.toFixed( isinteger( after ) ? after : int.toString().length ) );
        } );
        
        /**
         * @readonly
         * @function wrapint
         * @param {number} int
         * @param {number} howmany
         * @return {!number}
         * */
        _defineProperty( global , 'wrapinteger' , function ( int , howmany ) {
            int = int.toString();
            
            while ( int.length < howmany ) {
                int = '0' + int;
            }
            
            return int;
        } );
        
        /**
         * @readonly
         * @function jsonparse
         * @param {string} str
         * @return {?Object}
         * */
        _defineProperty( global , 'jsonparse' , function ( str ) {
            var result = null ,
                masterKey;
            
            try {
                result = JSON.parse( str );
            } catch ( e ) {
                try {
                    masterKey = randomstring();
                    
                    global[ masterKey ] = null;
                    
                    eval( 'global[ masterKey ] = ' + str );
                    
                    if ( isobject( global[ masterKey ] ) ) {
                        result = global[ masterKey ] || null;
                    }
                } catch ( e ) {
                    result = null;
                } finally {
                    delete global[ masterKey ];
                }
            }
            
            return result;
        } );
        
        if ( !isfunction( global.setImmediate ) ) {
            ( function () {
                var message = 0 ,
                    register = {} ,
                    prefix = randomstring() + '.immediate';
                
                function getLocation ( location ) {
                    try {
                        if ( isfillstring( location.origin ) && location.origin !== 'null' ) {
                            return location.origin;
                        }
                        
                        if ( location.ancestorOrigins && location.ancestorOrigins.length ) {
                            return location.ancestorOrigins[ 0 ];
                        }
                    } catch ( _ ) {
                        return null;
                    }
                }
                
                var origin = ( function () {
                    var init = global;
                    
                    while ( true ) {
                        var tmp = getLocation( init.location );
                        
                        if ( tmp ) {
                            return tmp;
                        }
                        
                        if ( iswindow( init.parent ) ) {
                            init = init.parent;
                            continue;
                        }
                        
                        return '';
                    }
                } )();
                
                try {
                    global.postMessage( prefix + 1 , function () {} );
                } catch ( e ) {
                    return ( function () {
                        _defineProperty( global , 'setImmediate' , global.setTimeout );
                        _defineProperty( global , 'clearImmediate' , global.clearTimeout );
                    } )();
                }
                
                global.addEventListener( 'message' , function ( event ) {
                    if ( origin === event.origin ) {
                        var i = event.data;
                        
                        if ( isfunction( register[ i ] ) ) {
                            register[ i ]();
                            delete register[ i ];
                            event.stopImmediatePropagation();
                        }
                    }
                } , eventPassiveSupport ? { capture : true , passive : true } : true );
                
                /**
                 * @readonly
                 * @function setImmediate
                 * @param {function} fn
                 * @return {!number}
                 * */
                _defineProperty( global , 'setImmediate' , function ( fn ) {
                    var id = ++message;
                    
                    register[ prefix + id ] = fn;
                    global.postMessage( prefix + id , origin );
                    
                    return id;
                } );
                
                /**
                 * @readonly
                 * @function clearImmediate
                 * @param {number} id
                 * @return void
                 * */
                _defineProperty( global , 'clearImmediate' , function ( id ) {
                    if ( register[ prefix + id ] ) {
                        delete register[ prefix + id ];
                    }
                } );
            } )();
        }
        
        /**
         * @readonly
         * @function getstyleproperty
         * @param {HTMLElement|Element|Node} element
         * @param {string} property
         * @return {?string}
         * */
        _defineProperty( global , 'getstyleproperty' , function ( element , property ) {
            var val , win;
            
            if ( isnode( element ) ) {
                
                if ( val = element.style[ property ] ) {
                    return val;
                }
                
                if ( val = element.style.getPropertyValue( property ) ) {
                    return val;
                }
                
                if ( win = getwindow( element ) ) {
                    return win.getComputedStyle( element ).getPropertyValue( property ) || null;
                }
                
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function splitTrim
         * @param {string} str
         * @param {RegExp|string} spl
         * @return {Array}
         * */
        _defineProperty( global , 'splitTrim' , function ( str , spl ) {
            var ar = str.split( spl );
            
            for ( var i = 0 , r = [] , tmp ; i < ar.length ; ) {
                if ( tmp = ar[ i++ ].trim() ) {
                    r.push( tmp );
                }
            }
            
            return r;
        } );
        
        /**
         * @readonly
         * @function clonearray
         * @param {Array} _
         * @return {!Array}
         * */
        _defineProperty( global , 'clonearray' , function ( _ ) {
            return _.slice( 0 );
        } );
        
        /**
         * @readonly
         * @function toarray
         * @param {*} _
         * @return {!Array}
         * */
        _defineProperty( global , 'toarray' , function ( _ ) {
            return isset( _ ) ? ( isarray( _ ) ? _ : [ _ ] ) : [];
        } );
        
        /**
         * @readonly
         * @function counterCallback
         * @param {number} counter
         * @param {function} callback
         * @return {!Array}
         * */
        _defineProperty( global , 'counterCallback' , function ( counter , callback ) {
            var current = 0;
            
            return function () {
                if ( ++current == counter ) {
                    callback();
                }
            };
        } );
        
        /**
         * @readonly
         * @function regexpEscapeString
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'regexpEscapeString' , function ( str ) {
            if ( exist( str ) ) {
                return str.toString()
                    .replace( /\\/gi , '\\\\' )
                    .replace( /([.\[\](){}^?*|+\-$])/gi , '\\$1' );
            }
            
            return '';
        } );
        
        /**
         * @readonly
         * @function htmlEscapeString
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'htmlEscapeString' , function ( str ) {
            if ( exist( str ) ) {
                return str.toString()
                    .replace( /&/gi , '&amp;' )
                    .replace( /"/gi , '&quot;' )
                    .replace( /'/gi , '&apos;' )
                    .replace( /</gi , '&lt;' )
                    .replace( />/gi , '&gt;' );
            }
            
            return '';
        } );
        
        /**
         * @readonly
         * @function resetregexp
         * @param {RegExp} _
         * @return {?RegExp}
         * */
        _defineProperty( global , 'resetregexp' , function ( _ ) {
            if ( !isregexp( _ ) ) {
                return null;
            }
            
            return _.lastIndex = _.index = 0, _;
        } );
        
        /**
         * @readonly
         * @function overrideObject
         * @return {!Object}
         * */
        _defineProperty( global , 'overrideObject' , function () {
            var result = {} ,
                array = typedtoarray( arguments );
            
            if ( array.length <= 0 ) {
                return result;
            }
            
            if ( array.length == 1 ) {
                return isobject( array[ 0 ] ) ? array[ 0 ] : result;
            }
            
            for ( var i = 0 , l = array.length ; i < l ; i++ ) {
                forin( array[ i ] , function ( key , value ) {
                    result[ key ] = value;
                } );
            }
            
            return result;
        } );
        
        /**
         * @readonly
         * @function isEmptyObject
         * @param {Object} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isEmptyObject' , function ( _ ) {
            if ( isobject( _ ) ) {
                for ( var key in _ ) {
                    if ( hasProperty.call( _ , key ) ) {
                        return false;
                    }
                }
                
                return true;
            }
            
            return false;
        } );
        
        /**
         * @readonly
         * @function forin
         * @param {Object} obj
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'forin' , function ( obj , callback ) {
            var key , value;
            
            if ( isobject( obj ) ) {
                for ( key in obj ) {
                    if ( hasProperty.call( obj , key ) && isset( value = obj[ key ] ) ) {
                        callback( key , value , obj );
                    }
                }
            }
        } );
        
        /**
         * @readonly
         * @function foreach
         * @param {Array} arr
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'foreach' , function ( arr , callback ) {
            if ( isarray( arr ) ) {
                for ( var i = 0 , l = arr.length ; i < l ; i++ ) {
                    callback( arr[ i ] , i , arr );
                }
            }
        } );
        
        /**
         * @readonly
         * @function for
         * @param {Array} arg
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'for' , function ( arg , callback ) {
            if ( isarray( arg ) ) {
                foreach( arg , callback );
            }
            else if ( isobject( arg ) ) {
                forin( arg , callback );
            }
        } );
        
        /**
         * @readonly
         * @function smoothyIncrement
         * @param {Object} setting
         * @param {number} setting.begin
         * @param {number} setting.end
         * @param {function} setting.eachFrame
         * @param {function} [setting.onFinish]
         * @param {number} [setting.speed=500]
         * @return void
         * */
        _defineProperty( global , 'smoothyIncrement' , ( function () {
            
            var frameLatency = 1000 / 60;
            
            var requestFrame = global[ 'requestAnimationFrame' ] ||
                global[ 'webkitRequestAnimationFrame' ] ||
                global[ 'mozRequestAnimationFrame' ] ||
                global[ 'msRequestAnimationFrame' ];
            
            var cancelFrame = global[ 'cancelAnimationFrame' ] ||
                global[ 'webkitCancelAnimationFrame' ] ||
                global[ 'mozCancelAnimationFrame' ] ||
                global[ 'msCancelAnimationFrame' ];
            
            function ease ( n ) {
                return 0.5 * ( 1 - Math.cos( Math.PI * n ) );
            }
            
            function animate ( callframe ) {
                var frame , start;
                
                function loop () {
                    frame = requestFrame( loop );
                    callframe();
                }
                
                start = setImmediate( loop );
                
                return function () {
                    clearImmediate( start );
                    cancelFrame( frame );
                };
            }
            
            return function ( setting ) {
                
                setting = overrideObject( {
                    speed : 1000
                } , setting );
                
                var begin = setting.begin ,
                    end = setting.end ,
                    speed = setting.speed ,
                    callback = setting.eachFrame ,
                    callbackFinish = setting.onFinish;
                
                if ( !isinteger( speed ) || speed <= 0 ) {
                    speed = 1000;
                }
                
                speed = Math.round( speed / 1.8 );
                
                var tmp = begin;
                begin = Math.min( begin , end );
                end = Math.max( tmp , end );
                
                var newval , elapsed;
                
                var firstStep = true ,
                    lastStep = false ,
                    endAtNext = false;
                
                var beginValues = [] ,
                    beginTotal = 0 ,
                    total = 0;
                
                var elapsedMax = 1 ,
                    elapsedMin = 0.1;
                
                var totalScroll = end - begin ,
                    increment = Math.round( totalScroll / ( speed / frameLatency ) );
                
                if ( totalScroll <= increment ) {
                    return callback( end , end - begin );
                }
                
                var startTime = Date.now();
                
                var stop = animate( function () {
                    
                    /* stop process */
                    /* ---------------------- */
                    if ( endAtNext ) {
                        callback( end , end - begin );
                        
                        if ( isfunction( callbackFinish ) ) {
                            callbackFinish();
                        }
                        
                        return stop();
                    }
                    
                    /* calc process */
                    /* ---------------------- */
                    
                    elapsed = round( ( Date.now() - startTime ) / speed , 2 );
                    
                    if ( elapsed < elapsedMin ) {
                        elapsed = elapsedMin;
                    }
                    else if ( elapsed > elapsedMax ) {
                        elapsed = elapsedMax;
                    }
                    
                    if ( firstStep && elapsed == elapsedMax ) {
                        firstStep = false;
                    }
                    
                    /* begin process */
                    /* ---------------------- */
                    
                    newval = Math.round( increment * ease( elapsed ) );
                    
                    if ( newval < 1 ) {
                        newval = 1;
                    }
                    
                    /* middle process */
                    /* ---------------------- */
                    
                    if ( firstStep ) {
                        beginValues.push( newval );
                        beginTotal += newval;
                    }
                    
                    if ( !firstStep && !lastStep && ( totalScroll - total ) <= beginTotal ) {
                        lastStep = true;
                    }
                    
                    /* end process */
                    /* ---------------------- */
                    
                    if ( lastStep ) {
                        if ( beginValues.length ) {
                            newval = beginValues.pop();
                        }
                        else {
                            newval = 1;
                        }
                    }
                    
                    total += newval;
                    
                    /* request stop process */
                    /* ---------------------- */
                    
                    if ( lastStep && totalScroll - total <= newval ) {
                        endAtNext = true;
                    }
                    
                    /* callback */
                    /* ---------------------- */
                    
                    callback( begin += newval , newval );
                    
                } );
                
                return stop;
                
            };
            
        } )() );
        
        /**
         * @readonly
         * @function cacheHandler
         * @param {number} [n=500]
         * @return {!function}
         * */
        _defineProperty( global , 'cacheHandler' , function ( n ) {
            var c = [];
            !n && ( n = 500 );
            
            function SAVE ( k , v ) {
                if ( !/^_(rm|purge)$/g.test( k ) ) {
                    SAVE[ k ] = v;
                    c.push( k );
                    
                    if ( c.length > n ) {
                        delete SAVE[ c.shift() ];
                    }
                }
            }
            
            SAVE._rm = function ( k ) {
                var i;
                
                if ( ( i = c.indexOf( k ), i ) !== -1 ) {
                    delete SAVE[ k ];
                    c.splice( i , 1 );
                }
            };
            
            SAVE._purge = function () {
                var i = c.length;
                
                while ( i-- ) {
                    delete SAVE[ c.shift() ];
                }
            };
            
            return SAVE;
        } );
        
        /**
         * @readonly
         * @function requireJS
         * @param {string|Array} source
         * @param {function} callback
         * @param {Document} [document=Document]
         * @return void
         * */
        _defineProperty( global , 'requireJS' , ( function () {
            var doc = getdocument( global );
            
            function require ( src , callback , document ) {
                var script;
                
                
                function load ( e ) {
                    callback();
                    e && e.type == 'load' && script.removeEventListener( 'load' , load );
                }
                
                if ( document ) {
                    
                    if ( document.querySelector( 'script[src="' + src + '"]' ) ) {
                        return load();
                    }
                    
                    script = document.createElement( 'script' );
                    
                    script.setAttribute( 'src' , src );
                    script.setAttribute( 'async' , 'async' );
                    script.setAttribute( 'type' , 'text/javascript' );
                    
                    script.addEventListener( 'load' , load );
                    
                    document.head.appendChild( script );
                    
                }
            }
            
            return function ( source , callback , document ) {
                var scripts = toarray( source ) ,
                    l = scripts.length ,
                    i = 0;
                
                function loop () {
                    if ( i < l ) {
                        return require( scripts[ i++ ] , loop , document || doc );
                    }
                    
                    if ( isfunction( callback ) ) {
                        callback();
                    }
                }
                
                loop();
            };
            
        } )() );
        
        /**
         * @readonly
         * @function requireCSS
         * @param {string|Array} source
         * @param {function} callback
         * @param {Document} [document=Document]
         * @return void
         * */
        _defineProperty( global , 'requireCSS' , ( function () {
            var doc = getdocument( global );
            
            function require ( href , callback , document ) {
                var link;
                
                function load ( e ) {
                    callback();
                    e && e.type == 'load' && link.removeEventListener( 'load' , load );
                }
                
                if ( document ) {
                    
                    if ( document.querySelector( 'link[href="' + href + '"]' ) ) {
                        return load();
                    }
                    
                    link = document.createElement( 'link' );
                    
                    link.setAttribute( 'href' , href );
                    link.setAttribute( 'rel' , 'stylesheet' );
                    
                    link.addEventListener( 'load' , load );
                    
                    document.head.appendChild( link );
                    
                }
            }
            
            return function ( source , callback , document ) {
                var scripts = toarray( source ) ,
                    l = scripts.length ,
                    i = 0;
                
                function loop () {
                    if ( i < l ) {
                        return require( scripts[ i++ ] , loop , document || doc );
                    }
                    
                    if ( isfunction( callback ) ) {
                        callback();
                    }
                }
                
                loop();
            };
            
        } )() );
        
        /**
         * @readonly
         * @function htmlDOM
         * @param {string} str
         * @return {?(HTMLElement|Element|Node|array)}
         * */
        _defineProperty( global , 'htmlDOM' , ( function () {
            var document = getdocument( global );
            
            if ( !document ) {
                return function () {
                    return null;
                };
            }
            
            var parentOf = {
                "col" : "colgroup" ,
                "tr" : "tbody" ,
                "th" : "tr" ,
                "td" : "tr" ,
                "colgroup" : "table" ,
                "tbody" : "table" ,
                "thead" : "table" ,
                "tfoot" : "table" ,
                "dt" : "dl" ,
                "dd" : "dl" ,
                "figcaption" : "figure" ,
                "legend" : "fieldset" ,
                "fieldset" : "form" ,
                "keygen" : "form" ,
                "area" : "map" ,
                "menuitem" : "menu" ,
                "li" : "ul" ,
                "option" : "optgroup" ,
                "optgroup" : "select" ,
                "output" : "form" ,
                "rt" : "ruby" ,
                "rp" : "ruby" ,
                "summary" : "details" ,
                "track" : "video" ,
                "source" : "video" ,
                "param" : "object"
            };
            
            var _ = cacheHandler();
            
            function clone ( e ) {
                if ( isarray( e ) ) {
                    for ( var i = 0 , l = e.length , r = [] ; i < l ; i++ ) {
                        r.push( e[ i ].cloneNode( true ) );
                    }
                    return r;
                }
                
                if ( exist( e ) ) {
                    return e.cloneNode( true );
                }
                
                return null;
            }
            
            function child ( e ) {
                return e.children.length == 1 ? e.children[ 0 ] : Array.apply( null , e.children );
            }
            
            function createHTML ( str ) {
                var firstTag = ( /^(<[\s]*[\w]{1,15}[\s]*(\b|>)?)/gi.exec( str ) || [ '' ] )[ 0 ]
                    .replace( /\W/g , '' ).trim().toLowerCase() ,
                    parent , doc;
                
                if ( !firstTag ) {
                    return;
                }
                
                if ( !parentOf[ firstTag ] ) {
                    doc = document.createDocumentFragment();
                    parent = document.createElement( 'DIV' );
                    doc.appendChild( parent );
                    parent.innerHTML = str;
                    return child( parent );
                }
                
                /** @type {Node} */
                parent = createHTML( '<' + parentOf[ firstTag ] + '></' + parentOf[ firstTag ] + '>' );
                
                while ( parent.firstChild ) {
                    parent = parent.firstChild;
                }
                
                parent.innerHTML = str;
                
                return child( parent );
            }
            
            return function ( str ) {
                var r;
                
                str = str.trim();
                
                if ( _[ str ] ) {
                    return clone( _[ str ] );
                }
                
                if ( !/(<[\s]*(\/)?[\s]*[\w]{1,15}[\s]*(\/)?[\s]*(\b|>)?)/gi.test( str ) ) {
                    return null;
                }
                
                if ( /^(<\/[\s]*[\w]{1,15}[\s]*>)$/gi.test( str ) ) {
                    r = document.createElement( str.replace( /\W/g , '' ).toUpperCase() );
                }
                else {
                    r = createHTML( str );
                }
                
                return _( str , r ) , clone( r );
            };
        } )() );
        
    } )();
    
    /**
     * targetJS
     * @license MIT
     * @see {@link https://framagit.org/tla/targetJS}
     * */
    ( function () {
        
        var _tmatch ,
            _tclosest ,
            level = 0 ,
            concat = Array.prototype.push;
        
        // cache
        var lvl = cacheHandler() ,
            findCache = cacheHandler() ,
            multiCache = cacheHandler();
        
        function getHtml ( e ) {
            return getdocument( e ).documentElement;
        }
        
        // return "ar2" items - "ar" items
        function filterDiff ( ar , ar2 ) {
            var r = [];
            
            if ( ar.length ) {
                for ( var i = 0 , l = ar2.length ; i < l ; i++ ) {
                    if ( !inarray( ar2[ i ] , ar ) ) {
                        r.push( ar2[ i ] );
                    }
                }
            }
            
            return r;
        }
        
        function nodelistFilter ( nodelist , filter , reverse ) {
            for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                node = nodelist[ i ];
                
                if ( _tmatch( node , filter , node.parentNode ) ) {
                    r.push( node );
                }
            }
            
            return reverse ? r.reverse() : r;
        }
        
        function globalNodelistFilter ( nodelist , filter ) {
            for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                node = nodelist[ i ];
                
                if ( _tmatch( node , filter ) ) {
                    r.push( node );
                }
            }
            
            return r;
        }
        
        var applyToolsOnNodelist = ( function () {
            var first = /^([*.:"'{})(=~|^$+#>a-z0-9\[\]-]+)(\b)?/gi;
            
            function firstLevel ( selector ) {
                var r = lvl[ selector ];
                
                if ( r ) {
                    return r;
                }
                
                r = resetregexp( first ).exec( selector )[ 0 ];
                lvl( selector , r );
                
                return r;
            }
            
            return function ( nodelist , filter ) {
                var i = 0 , l = nodelist.length , n , p , r = [] ,
                    compiled = compiler( filter , '*' , filter + '*' )[ 0 ][ 0 ] ,
                    s = firstLevel( compiled[ 0 ] );
                
                for ( i = 0 ; i < l ; i++ ) {
                    n = nodelist[ i ] , p = n.parentNode;
                    
                    if ( _tmatch( n , firstLevel( s ) , p ) ) {
                        concat.apply( r , advanceRequest( {
                            container : p ,
                            nodelist : [ n ] ,
                            tool : compiled[ 1 ] ,
                            selector : compiled[ 0 ]
                        } ) );
                    }
                }
                
                return r;
            };
        } )();
        
        var animated = [];
        ( function ( event , listenerArgument ) {
            if ( event && isfunction( global.addEventListener ) ) {
                
                event.slice( 0 , 2 ).forEach( function ( e ) {
                    global.addEventListener( e , function ( e ) {
                        animated.push( e.target );
                    } , listenerArgument );
                } );
                
                event.slice( 2 ).forEach( function ( e ) {
                    global.addEventListener( e , function ( e ) {
                        var i;
                        if ( ( i = animated.indexOf( e.target ) , i ) !== -1 ) {
                            animated.splice( i , 1 );
                        }
                    } , listenerArgument );
                } );
                
            }
        } )(
            // animation events
            ( function () {
                var test = document.createElement( 'DIV' ).style ,
                    
                    pre = isset( test[ 'animation' ] ) && [
                            'animationstart' , 'transitionstart' , 'animationend' , 'transitionend '
                        ] ||
                        ( isset( test[ 'oAnimation' ] ) || isset( test[ 'OAnimation' ] ) ) && [
                            'oanimationstart' , 'otransitionstart' , 'oanimationend' , ' otransitionend'
                        ] ||
                        isset( test[ 'webkitAnimation' ] ) && [
                            'webkitAnimationstart' , 'webkitTransitionstart' , 'webkitAnimationEnd' , 'webkitTransitionEnd'
                        ] || null;
                
                return test = null, pre;
            } )() ,
            
            // passive event support
            eventPassiveSupport ? { capture : true , passive : true } : true
        );
        
        function childrenNodes ( element , r ) {
            var c = typedtoarray( element.children );
            return r ? c.reverse() : c;
        }
        
        var onlyChildsOf = ( function () {
            
            function isParent ( e , p ) {
                return p !== e && p.contains( e );
            }
            
            return function ( parent , nodelist ) {
                for ( var i = 0 , l = nodelist.length , r = [] , tmp ; i < l ; i++ ) {
                    if ( tmp = nodelist[ i ] , isParent( tmp , parent ) ) {
                        r.push( tmp );
                    }
                }
                return r;
            };
        } )();
        
        var animationState = ( function () {
            var animationStateProp = ( function () {
                var test = document.createElement( 'DIV' ).style ,
                    pre = isset( test[ 'animationPlayState' ] ) ? '' :
                        isset( test[ 'OAnimationPlayState' ] ) ? '-o-' :
                            isset( test[ 'MozAnimationPlayState' ] ) ? '-moz-' :
                                isset( test[ 'WebkitAnimationPlayState' ] ) ? '-webkit-' : '';
                return test = null , pre;
            } )() + 'animation-play-state';
            
            return function ( node ) {
                return getstyleproperty( node , animationStateProp );
            };
        } )();
        
        // find index of f in s
        // ex f = ","
        // "div , p" return [5]
        // but "div:not(a,p)" return [0]
        function multiIndexOf ( f , s ) {
            var open , p , i , c , tmp = f + '_' + s;
            
            if ( multiCache[ tmp ] ) {
                return multiCache[ tmp ];
            }
            
            for ( open = 0, p = [], i = 0 ; c = s.charAt( i ) ; i++ ) {
                if ( /([(\[])/g.exec( c ) ) {
                    open++;
                }
                else if ( /([)\]])/g.exec( c ) ) {
                    open--;
                }
                else if ( c === f && !open ) {
                    p.push( i );
                }
            }
            
            return multiCache( tmp , p.reverse() ), multiCache[ tmp ];
        }
        
        // like multiIndexOf but return only first index of "(" ( or null )
        function firstIndexOf ( f , s ) {
            for ( var open = 0 , p = null , i = 0 , c ; c = s.charAt( i ) ; i++ ) {
                if ( /([(\[])/g.exec( c ) ) {
                    open++;
                }
                else if ( /([)\]])/g.exec( c ) ) {
                    open--;
                }
                else if ( c === f && !open ) {
                    p = i;
                    break;
                }
            }
            
            return p;
        }
        
        // like multiIndexOf but return last index of ")" ( or null )
        function findCloser ( s ) {
            for ( var open = 0 , p = null , i = 0 , c ; c = s.charAt( i ) ; i++ ) {
                if ( /([(\[])/g.exec( c ) ) {
                    open++;
                }
                else if ( /([)\]])/g.exec( c ) ) {
                    if ( !--open && c === ')' ) {
                        p = i;
                        break;
                    }
                }
            }
            
            return p;
        }
        
        // check if a css tool is natively supported
        // ex :
        // support('checked') can be true
        // support('nth-of-type') too
        // support('yolo') return false...
        function support ( flag , b ) {
            try {
                return document.querySelector( ':' + flag ), true;
            } catch ( _ ) {
                return b ? false : support( flag + '(0)' , true );
            }
        }
        
        // css selector
        // use getElementById / getElementsByClassName / getElementsByTagName if it's possible, else querySelectorAll
        var query = ( function () {
            var qsa = function ( s , c ) {
                    return c.querySelectorAll( s );
                } ,
                by = {
                    "#" : function ( i ) {
                        return document.getElementById( i );
                    } ,
                    "." : document.getElementsByClassName ? function ( i , c ) {
                        return c.getElementsByClassName( i );
                    } : function ( i , c ) {
                        return qsa( '.' + i , c );
                    }
                } ,
                hard = /[\s:.)\[><~^+]+/g;
            
            function get ( s , c ) {
                var t = s.charAt( 0 ) , l = s.slice( 1 ) , b = ( t == '.' ) , tmp;
                
                if ( ( b && resetregexp( hard ).exec( l ) ) || ( !b && resetregexp( hard ).exec( s ) ) ) {
                    return qsa( s , c );
                }
                
                if ( tmp = by[ t ] ) {
                    return tmp( l , c );
                }
                
                return c.getElementsByTagName( s );
            }
            
            return function ( s , c ) {
                
                switch ( s ) {
                    
                    case 'document':
                        return [ global.document ];
                    
                    case 'window':
                        return [ global ];
                    
                }
                
                var tmp = get( s , c );
                
                if ( exist( tmp ) ) {
                    
                    if ( exist( tmp.length ) ) {
                        return typedtoarray( tmp );
                    }
                    
                    return [ tmp ];
                }
                
                return [];
            };
        } )();
        
        // targetJS features
        var advSelector = {
            
            'root' : function ( n , f , s ) {
                return [ getHtml( s.container ) ];
            } ,
            
            "before-all" : function ( nodelist , flag ) {
                if ( !flag ) {
                    return [];
                }
                
                for ( var i = 0 , l = nodelist.length , r = [] , node , next ; i < l ; i++ ) {
                    node = nodelist[ i ];
                    next = node.nextElementSibling;
                    
                    while ( next ) {
                        if ( _tmatch( next , flag ) ) {
                            r.push( node );
                            break;
                        }
                        next = next.nextElementSibling;
                    }
                }
                
                return r;
            } ,
            
            "after-all" : function ( nodelist , flag ) {
                if ( !flag ) {
                    return [];
                }
                
                for ( var i = 0 , l = nodelist.length , r = [] , node , next ; i < l ; i++ ) {
                    node = nodelist[ i ];
                    next = node.previousElementSibling;
                    
                    while ( next ) {
                        if ( _tmatch( next , flag ) ) {
                            r.push( node );
                            break;
                        }
                        next = next.previousElementSibling;
                    }
                }
                
                return r;
            } ,
            
            "before" : function ( nodelist , flag ) {
                if ( !flag ) {
                    return [];
                }
                
                for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                    node = nodelist[ i ];
                    if ( _tmatch( node.nextElementSibling , flag ) ) {
                        r.push( node );
                    }
                }
                
                return r;
            } ,
            
            "after" : function ( nodelist , flag ) {
                if ( !flag ) {
                    return [];
                }
                
                for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                    node = nodelist[ i ];
                    if ( _tmatch( node.previousElementSibling , flag ) ) {
                        r.push( node );
                    }
                }
                
                return r;
            } ,
            
            'animated' : function ( nodelist ) {
                for ( var i = 0 , l = nodelist.length , r = [] ; i < l ; i++ ) {
                    if ( inarray( nodelist[ i ] , animated ) && animationState( nodelist[ i ] ) !== "paused" ) {
                        r.push( nodelist[ i ] );
                    }
                }
                return r;
            } ,
            
            'contains-cs' : function ( nodelist , flag ) {
                if ( !flag ) {
                    return [];
                }
                
                var reg = new RegExp( '(' + regexpEscapeString( flag ) + ')' , 'gi' );
                for ( var i = 0 , l = nodelist.length , r = [] ; i < l ; i++ ) {
                    if ( resetregexp( reg ).exec( nodelist[ i ].textContent || nodelist[ i ].innerText ) ) {
                        r.push( nodelist[ i ] );
                    }
                }
                
                return r;
            } ,
            
            'contains' : function ( nodelist , flag ) {
                if ( !flag ) {
                    return [];
                }
                
                var reg = new RegExp( '(' + regexpEscapeString( flag ) + ')' , 'g' );
                for ( var i = 0 , l = nodelist.length , r = [] ; i < l ; i++ ) {
                    if ( resetregexp( reg ).exec( nodelist[ i ].textContent || nodelist[ i ].innerText ) ) {
                        r.push( nodelist[ i ] );
                    }
                }
                
                return r;
            } ,
            
            'first' : function ( nodelist , n , obj ) {
                return obj.selector == '*' ? nodelistFilter( nodelist , '' ) :
                    nodelist.length ? [ nodelist[ 0 ] ] : [];
            } ,
            
            'last' : function ( nodelist , n , obj ) {
                return obj.selector == '*' ? nodelistFilter( nodelist , ':last-child' ) :
                    nodelist.length ? [ nodelist[ nodelist.length - 1 ] ] : [];
            } ,
            
            'is-parent' : function ( nodelist ) {
                for ( var i = 0 , r = [] , l = nodelist.length ; i < l ; i++ ) {
                    if ( childrenNodes( nodelist[ i ] ).length ) {
                        r.push( nodelist[ i ] );
                    }
                }
                return r;
            } ,
            
            'is-child-of' : function ( nodelist , flag ) {
                if ( !flag ) {
                    return [];
                }
                
                for ( var i = 0 , r = [] , l = nodelist.length , node ; i < l ; i++ ) {
                    if ( isnode( node = nodelist[ i ].parentNode ) && _tmatch( node , flag ) ) {
                        r.push( nodelist[ i ] );
                    }
                }
                
                return r;
            } ,
            
            'draggable' : function ( nodelist ) {
                for ( var i = 0 , l = nodelist.length , r = [] ; i < l ; i++ ) {
                    if ( nodelist[ i ].draggable ) {
                        r.push( nodelist[ i ] );
                    }
                }
                return r;
            } ,
            
            'is' : function ( nodelist , flag ) {
                if ( !flag ) {
                    return [];
                }
                
                return nodelistFilter( nodelist , flag );
            } ,
            
            'has' : function ( nodelist , flag ) {
                if ( !flag ) {
                    return [];
                }
                
                for ( var i = 0 , r = [] , l = nodelist.length , f = find ; i < l ; i++ ) {
                    if ( !!f( flag , nodelist[ i ] ).length ) {
                        r.push( nodelist[ i ] );
                    }
                }
                
                return r;
            } ,
            
            'find' : function ( nodelist , flag ) {
                if ( !flag ) {
                    return [];
                }
                
                for ( var i = 0 , r = [] , l = nodelist.length ; i < l ; ) {
                    concat.apply( r , find( flag , nodelist[ i++ ] ) );
                }
                
                return r;
            } ,
            
            'closest' : function ( nodelist , flag ) {
                for ( var i = 0 , l = nodelist.length , r = [] , node , tmp ; i < l ; i++ ) {
                    if ( !isnode( node = nodelist[ i ] , node ).parentNode ) {
                        continue;
                    }
                    
                    if ( flag ) {
                        if ( tmp = _tclosest( node.parentNode , flag ) ) {
                            r.push( tmp );
                        }
                        continue;
                    }
                    
                    while ( isnode( node = node.parentNode ) ) {
                        r.push( node );
                    }
                }
                return r;
            } ,
            
            'parent' : function ( nodelist , flag ) {
                for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                    if ( isnode( node = nodelist[ i ].parentNode ) &&
                        ( !flag || ( flag && _tmatch( node , flag ) ) ) ) {
                        r.push( node );
                    }
                }
                return r;
            } ,
            
            'value' : function ( nodelist ) {
                for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                    if ( ( isset( ( node = nodelist[ i ] ).value ) && !!node.value.toString().trim() ) ) {
                        r.push( node );
                    }
                }
                return r;
            } ,
            
            'no-value' : function ( nodelist ) {
                for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                    if ( ( isset( ( node = nodelist[ i ] ).value ) && !node.value.toString().trim() ) ) {
                        r.push( node );
                    }
                }
                return r;
            } ,
            
            'unchecked' : function ( nodelist ) {
                for ( var i = 0 , l = nodelist.length , r = [] , t , n ; i < l ; i++ ) {
                    t = nodelist[ i ], n = t.tagName.toLowerCase();
                    
                    if ( ( n == 'input' && !t.checked ) || ( n == 'option' && !t.selected ) ) {
                        r.push( t );
                    }
                }
                return r;
            } ,
            
            'selected' : function ( nodelist ) {
                for ( var i = 0 , l = nodelist.length , r = [] , t ; i < l ; i++ ) {
                    t = nodelist[ i ];
                    
                    if ( t.tagName.toLowerCase() == 'option' && t.selected ) {
                        r.push( t );
                    }
                }
                return r;
            } ,
            
            'unselected' : function ( nodelist ) {
                for ( var i = 0 , l = nodelist.length , r = [] , t ; i < l ; i++ ) {
                    t = nodelist[ i ];
                    
                    if ( t.tagName.toLowerCase() == 'option' && !t.selected ) {
                        r.push( t );
                    }
                }
                return r;
            } ,
            
            'blur' : function ( nodelist , flag , s ) {
                var a = getdocument( s.container ).activeElement;
                return filterDiff( a ? [ a ] : query( ':focus' , s.container ) , nodelist );
            } ,
            
            'not' : function ( nodelist , flag , setting ) {
                if ( !flag ) {
                    return [];
                }
                return filterDiff(
                    !/[:(\[><~+]/gi.exec( flag ) ?
                        query( flag , setting.container ) :
                        ( find( flag , setting.container ) || [] )
                    , nodelist
                );
            } ,
            
            'next' : function ( nodelist , flag , s ) {
                for ( var node , i = 0 , l = nodelist.length , r = [] , html = getHtml( s.container ) ; i < l ; i++ ) {
                    node = nodelist[ i ].nextElementSibling;
                    
                    if ( node && ( !flag || ( flag && _tmatch( node , flag , node.parentNode || html ) ) ) ) {
                        r.push( node );
                    }
                }
                return r;
            } ,
            
            'previous' : function ( nodelist , flag , s ) {
                for ( var node , i = 0 , l = nodelist.length , r = [] , html = getHtml( s.container ) ; i < l ; i++ ) {
                    node = nodelist[ i ].previousElementSibling;
                    
                    if ( node && ( !flag || ( flag && _tmatch( node , flag , node.parentNode || html ) ) ) ) {
                        r.push( node );
                    }
                }
                return r;
            } ,
            
            'next-all' : function ( nodelist , flag , s ) {
                for ( var node , i = 0 , l = nodelist.length , r = [] , html = getHtml( s.container ) , tmp ; i < l ; i++ ) {
                    node = nodelist[ i ];
                    
                    if ( flag ) {
                        tmp = [];
                        while ( node = node.nextElementSibling ) {
                            if ( !flag || ( flag && _tmatch( node , flag , node.parentNode || html ) ) ) {
                                tmp.push( node );
                            }
                        }
                        concat.apply( r , tmp );
                    }
                    else {
                        tmp = childrenNodes( node.parentNode );
                        concat.apply( r , tmp.slice( tmp.indexOf( node ) + 1 ) );
                    }
                }
                return r;
            } ,
            
            'previous-all' : function ( nodelist , flag , s ) {
                for ( var node , i = 0 , l = nodelist.length , r = [] , html = getHtml( s.container ) , tmp ; i < l ; i++ ) {
                    node = nodelist[ i ];
                    
                    if ( flag ) {
                        tmp = [];
                        while ( node = node.previousElementSibling ) {
                            if ( _tmatch( node , flag , node.parentNode || html ) ) {
                                tmp.push( node );
                            }
                        }
                        concat.apply( r , tmp.reverse() );
                    }
                    else {
                        tmp = childrenNodes( node.parentNode );
                        concat.apply( r , tmp.slice( 0 , tmp.indexOf( node ) ) );
                    }
                }
                return r;
            } ,
            
            'children' : function ( nodelist , flag ) {
                for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                    if ( ( node = nodelist[ i ] ).firstChild ) {
                        concat.apply( r , flag ? nodelistFilter( childrenNodes( node ) , flag , true ) : childrenNodes( node ) );
                    }
                }
                return r;
            } ,
            
            // for library only
            // -------------------------------------------------
            
            "fullAttr" : function ( nodelist , flag ) {
                for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                    node = nodelist[ i ];
                    
                    if ( node.hasAttribute( flag ) && !!( node.getAttribute( flag ) || '' ).trim() ) {
                        r.push( node );
                    }
                }
                return r;
            } ,
            
            "emptyAttr" : function ( nodelist , flag ) {
                for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                    node = nodelist[ i ];
                    
                    if ( node.hasAttribute( flag ) && !( node.getAttribute( flag ) || '' ).trim() ) {
                        r.push( node );
                    }
                }
                return r;
            } ,
            
            "childNodes" : function ( nodelist , flag ) {
                for ( var i = 0 , l = nodelist.length , r = [] ; i < l ; i++ ) {
                    if ( nodelist[ i ].firstChild ) {
                        concat.apply( r , applyToolsOnNodelist( childrenNodes( nodelist[ i ] ) , flag ) );
                    }
                }
                return r;
            } ,
            
            "parentNode" : function ( nodelist , flag ) {
                for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                    if ( isnode( node = nodelist[ i ].parentNode ) ) {
                        r.push( node );
                    }
                }
                return applyToolsOnNodelist( r , flag );
            } ,
            
            "nextChildNodes" : function ( nodelist , flag ) {
                for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                    node = nodelist[ i ].nextElementSibling;
                    while ( node ) {
                        r.push( node );
                        node = node.nextElementSibling;
                    }
                }
                return applyToolsOnNodelist( r , flag );
            } ,
            
            "nextSibling" : function ( nodelist , flag ) {
                for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                    if ( node = nodelist[ i ].nextElementSibling ) {
                        r.push( node );
                    }
                }
                return applyToolsOnNodelist( r , flag );
            }
            
        };
        
        // css3 selector
        var crossBrowser = {
            
            'read-only' : function ( nodelist ) {
                for ( var i = 0 , l = nodelist.length , r = [] , t ; i < l ; i++ ) {
                    t = nodelist[ i ];
                    t.readOnly === true && r.push( t );
                }
                return r;
            } ,
            
            'read-write' : function ( nodelist ) {
                for ( var i = 0 , l = nodelist.length , r = [] , t ; i < l ; i++ ) {
                    t = nodelist[ i ];
                    t.readOnly === false && r.push( t );
                }
                return r;
            } ,
            
            'in-range' : function ( nodelist ) {
                var i = 0 , l = nodelist.length , r = [] ,
                    min , max , val , Emin , Emax;
                
                for ( ; i < l ; i++ ) {
                    min = nodelist[ i ].getAttribute( 'min' );
                    max = nodelist[ i ].getAttribute( 'max' );
                    val = nodelist[ i ].value;
                    Emin = exist( min );
                    Emax = exist( max );
                    
                    if ( exist( val ) && ( ( Emin && +val >= +min ) || !Emin ) && ( ( Emax && +val <= +max ) || !Emax ) ) {
                        r.push( nodelist[ i ] );
                    }
                }
                
                return r;
            } ,
            
            'out-of-range' : function ( nodelist ) {
                var i = 0 , l = nodelist.length , r = [] ,
                    min , max , val , Emin , Emax;
                
                for ( ; i < l ; i++ ) {
                    min = nodelist[ i ].getAttribute( 'min' );
                    max = nodelist[ i ].getAttribute( 'max' );
                    val = nodelist[ i ].value;
                    Emin = exist( min );
                    Emax = exist( max );
                    
                    if ( exist( val ) && ( ( Emin && +val < +min ) || !Emin ) && ( ( Emax && +val > +max ) || !Emax ) ) {
                        r.push( nodelist[ i ] );
                    }
                }
                
                return r;
            }
            
        };
        
        var compiler = ( function () {
            var compiler = cacheHandler() ,
                cacheNormalize = cacheHandler() ,
                
                // when the script load, check all supported css3 tool, if isn't, add the function in targetJS features object
                reg = ( function () {
                    var reg = [] , join;
                    
                    forin( crossBrowser , function ( selector , polyfill ) {
                        if ( !support( selector ) ) {
                            advSelector[ selector ] = polyfill;
                        }
                    } );
                    
                    forin( advSelector , function ( selector ) {
                        reg.push( selector );
                    } );
                    
                    join = reg.join( '|' );
                    
                    return {
                        tool : new RegExp( '^(' + join + ')$' , 'g' ) ,
                        check : new RegExp( '((:)(' + join + '))' , 'g' )
                    };
                } )() ,
                
                // check if selector is supported natively in :not()
                notSupport = ( function () {
                    function support ( flag ) {
                        try {
                            return document.querySelector( 'html:not(' + flag + ')' ), true;
                        } catch ( _ ) {
                            return false;
                        }
                    }
                    
                    return !support( 'body' ) ? function () {
                        return false;
                    } : function ( flag ) {
                        return support( flag );
                    };
                } )() ,
                
                // ex :
                // "p[!id]" become "p:not([id])"
                // "p[-id]" become "p:fullAttr([id])"
                // "p[!-id]" become "p:emptyAttr([id])"
                // "p:not(a,div)" become "p:not(a):not(div)"
                // ":first < :last" become ":first:parent(:last)"
                // ":first > :last" become ":first:children(:last)"
                // "p:not(a)[id!=toto]" become "p:not(a):not([id=toto])"
                beautify = ( function () {
                    var notEq = /(![=]{0,2})/gi ,
                        notEqB = /(\[![\w-]+])/gi ,
                        fullAttr = /(\[-[\w-]+])/gi ,
                        emptyAttr = /(\[!-[\w-]+])/gi ,
                        selec = '[* .:"\'\\\{}=~|\\\^\\\$\\\+#><a-z0-9\\\[\\\]-]+' ,
                        selecAdv = '[* .:"\'\\\{})(=~|\\\^\\\$\\\+#><a-z0-9\\\[\\\]-]+' ,
                        toEq = new RegExp( '(\\\[[\\\w-]+![=]{0,2}' + selec + '])' , 'gi' ) ,
                        multiSel = new RegExp( '(:not\\\()(' + selecAdv + ')([,]+' + selecAdv + '[,]?)+(\\\))' , 'gi' );
                    
                    function selToNot ( ar ) {
                        for ( var tmp , i = 0 , result = '' , l = ar.length ; i < l ; i++ ) {
                            ( tmp = ar[ i ].trim() ) && ( result += ':not(' + tmp + ')' );
                        }
                        return result;
                    }
                    
                    return function ( selector ) {
                        return extractSpec( selector.trim() )
                        // !=
                            .replace( toEq , function ( value ) {
                                return ':not(' + value.replace( notEq , '=' ) + ')';
                            } )
                            // [!-attr]
                            .replace( emptyAttr , function ( value ) {
                                return ':emptyAttr(' + value.slice( 3 , -1 ) + ')';
                            } )
                            // [-attr]
                            .replace( fullAttr , function ( value ) {
                                return ':fullAttr(' + value.slice( 2 , -1 ) + ')';
                            } )
                            // [!attr]
                            .replace( notEqB , function ( value ) {
                                return ':not([' + value.slice( 2 ) + ')';
                            } )
                            // multi not selector
                            .replace( multiSel , function ( value ) {
                                return selToNot( value.slice( 5 , -1 ).split( ',' ) );
                            } );
                    };
                } )() ,
                
                // split selector
                // first by comma
                // second by space if it's necessary ( ex : "div p" isn't splited but "div p:first" become ["div","p:first"]
                splitSelector = ( function () {
                    function commatSplit ( selector ) {
                        var i = 0 ,
                            ar = clonearray( multiIndexOf( ',' , selector ) ).reverse() ,
                            l = ar.length , cur = 0 , selectors = [] , tmp;
                        
                        for ( ; i < l ; i++ ) {
                            if ( tmp = selector.slice( cur , ar[ i ] ).trim() ) {
                                selectors.push( tmp );
                                cur = ar[ i ] + 1;
                            }
                        }
                        
                        if ( tmp = selector.slice( cur ).trim() ) {
                            selectors.push( tmp );
                        }
                        
                        return selectors;
                    }
                    
                    function spaceSplit ( selectors , parent ) {
                        var i = 0 , l = selectors.length , ar , cur = 0 ,
                            tmp , i2 , l2 , tmp2 , child , sel ,
                            spc = /[\s]*([~+><])[\s]*/g ,
                            beg = /^[\s]?[:\[]/g ,
                            reg = /[:)\[]+/g;
                        
                        for ( ; i < l ; i++ ) {
                            
                            ar = [];
                            child = '';
                            i2 = cur = 0;
                            
                            sel = selectors[ i ].replace( spc , function ( s ) {
                                return s.trim();
                            } );
                            tmp = clonearray( multiIndexOf( ' ' , sel ) ).reverse();
                            l2 = tmp.length;
                            
                            // TODO do...while instead of... that !
                            for ( ; i2 < l2 ; i2++ ) {
                                if ( tmp2 = sel.slice( cur , tmp[ i2 ] ).trim() ) {
                                    
                                    if ( resetregexp( reg ).exec( tmp2 ) ) {
                                        
                                        if ( resetregexp( beg ).exec( tmp2 = tmp2.trim() ) ) {
                                            tmp2 = parent + tmp2;
                                        }
                                        
                                        !!child && ( ar.push( child ), child = '' );
                                        
                                        ar.push( tmp2 );
                                    }
                                    else {
                                        child += ' ' + tmp2;
                                    }
                                    
                                    cur = tmp[ i2 ] + 1;
                                }
                            }
                            
                            if ( tmp2 = sel.slice( cur ).trim() ) {
                                
                                if ( resetregexp( reg ).exec( tmp2 ) ) {
                                    
                                    if ( resetregexp( beg ).exec( tmp2 = tmp2.trim() ) ) {
                                        tmp2 = parent + tmp2;
                                    }
                                    
                                    !!child && ( ar.push( child ), child = '' );
                                    
                                    ar.push( tmp2 );
                                }
                                else {
                                    child += ' ' + tmp2;
                                }
                                
                            }
                            
                            ( !ar.length || !!child ) && ar.push( child );
                            selectors[ i ] = ar;
                        }
                        
                        return selectors;
                    }
                    
                    return function ( str , parent ) {
                        return spaceSplit( commatSplit( normalize( str ) , parent ) , parent );
                    };
                } )() ,
                
                // input shortcut
                inputShortcut = ( function ( i , type ) {
                    return new RegExp( '((:)(' + type.join( '|' ) + '))' , 'gi' );
                } )( 0 , [
                    'checkbox' , 'text' , 'submit' , 'file' , 'email' , 'reset' , 'radio' , 'password' ,
                    'hidden' , 'color' , 'date' , 'datetime' , 'datetime-local' , 'image' , 'month' , 'number' ,
                    'range' , 'search' , 'tel' , 'time' , 'url' , 'week'
                ] , '' ) ,
                
                // css shortcut
                toolShortcut = ( function ( tool , reg ) {
                    for ( var i in tool ) {
                        reg += i + '|';
                    }
                    reg = RegExp( '((:)(' + reg.slice( 0 , -1 ) + '))' , 'gi' );
                    
                    function slice ( v ) { return tool[ v.slice( 1 ) ]; }
                    
                    return function ( str ) {
                        return str.replace( reg , slice );
                    };
                } )( {
                    odd : ':nth-child(odd)' ,
                    even : ':nth-child(even)'
                } , '' ) ,
                
                chkEnd = /[~>+<][\s]*$/g ,
                alpha = /[\w-]+/gi ,
                spec = /[><+~:]/gi;
            
            // remove useless space, in and around the string
            function normalize ( str ) {
                if ( cacheNormalize[ str ] ) {
                    return cacheNormalize[ str ];
                }
                
                cacheNormalize( str , str.replace( /[\s\u00A0]+/g , ' ' ).trim() );
                
                return cacheNormalize[ str ];
            }
            
            function switchTool ( char ) {
                var tool = null;
                
                switch ( char ) {
                    case '>':
                        tool = 'childNodes';
                        break;
                    
                    case '<':
                        tool = 'parentNode';
                        break;
                    
                    case '~':
                        tool = 'nextChildNodes';
                        break;
                    
                    case '+':
                        tool = 'nextSibling';
                        break;
                }
                
                return tool;
            }
            
            // parsing selector for ">", "<", "~", "+"
            // use native selector if it's possible
            function extractSpec ( selector ) {
                var i = 0 , l = selector.length ,
                    spc = /[><~+]/g , Hspc = /[<]/g ,
                    o = 0 , index = [] , r = [] , curIndex , befIndex ,
                    before , after , chk = reg.check , last = {} , laststack = [] , tool , char;
                
                for ( ; i < l ; i++ ) {
                    char = selector.charAt( i );
                    if ( char === '(' ) {
                        o++;
                    }
                    else if ( char === ')' ) {
                        o--;
                    }
                    else if ( resetregexp( spc ).exec( char ) && !o ) {
                        index.push( {
                            index : i + 1 ,
                            char : char ,
                            h : resetregexp( Hspc ).exec( char )
                        } );
                    }
                }
                
                for ( i = 0, index.reverse(), l = index.length ; i < l ; i++ ) {
                    curIndex = index[ i ].index;
                    char = index[ i ].char;
                    
                    befIndex = index[ i + 1 ] ? index[ i + 1 ].index : 0;
                    
                    before = selector.slice( befIndex , curIndex - 1 ).trim();
                    
                    after = selector.slice( curIndex ).trim();
                    
                    selector = selector.slice( 0 , curIndex - 1 ).trim();
                    
                    // standard selector, check input shortcut and goto next
                    if ( !resetregexp( chk ).exec( before ) && !resetregexp( chk ).exec( after ) && !index[ i ].h ) {
                        
                        r.unshift( char + after.replace( inputShortcut , function ( a ) {
                            return ' input[type="' + a.slice( 1 ) + '"]';
                        } ) );
                        
                        last = {
                            char : char ,
                            after : after ,
                            packed : false
                        };
                        
                        laststack.unshift( last );
                        
                        continue;
                    }
                    
                    // complex selector, switch to manual flag treatment
                    if ( tool = switchTool( char ) ) {
                        r.unshift( ':' + tool + '(' + after.trim() + ')' );
                        
                        last = {
                            char : char ,
                            after : after ,
                            packed : true
                        };
                        
                        laststack.unshift( last );
                        
                        // check last tool
                        // CAREFUL BETA
                        ( function ( i ) {
                            while ( r[ i ] && laststack[ i ] ) {
                                
                                if ( !laststack[ i ].packed ) {
                                    
                                    if ( tool = switchTool( laststack[ i ].char ) ) {
                                        r[ i ] = ':' + tool + '(' + laststack[ i ].after.trim() + ')';
                                        laststack[ i ].packed = true;
                                    }
                                    
                                }
                                else {
                                    break;
                                }
                                
                                i++;
                            }
                        } )( 1 );
                        
                        continue;
                    }
                    
                    last = null;
                }
                
                return normalize( selector + r.join( '' ) );
            }
            
            function badEnd ( s ) {
                return resetregexp( chkEnd ).exec( s ) ? s + '*' : s;
            }
            
            function checkTool ( selector ) {
                var b = 0 , l , i , i2 , ar , tmp , flag , tool ,
                    after , before , toolName , rest , r = [] , exp = reg.tool;
                
                // parse and replace input shortcut
                selector = selector.replace( inputShortcut , function ( a ) {
                    return ' input[type="' + a.slice( 1 ) + '"]';
                } );
                
                // parse and replace selector shortcut
                selector = toolShortcut( selector );
                
                // get all tools index
                ar = multiIndexOf( ':' , selector );
                
                for ( l = ar.length ; b < l ; b++ ) {
                    // tool ( with argument )
                    tool = selector.slice( ar[ b ] + 1 );
                    
                    if ( tmp = firstIndexOf( resetregexp( spec ) , tool ) ) {
                        tool = tool.slice( 0 , tmp );
                    }
                    
                    // tool name ( without argument )
                    toolName = resetregexp( alpha ).exec( tool )[ 0 ];
                    
                    // selector before tool
                    before = selector.slice( 0 , ar[ b ] );
                    
                    // selector after tool
                    after = selector.slice( ar[ b ] + tool.length + 1 );
                    
                    // argument
                    if ( ( i = tool.indexOf( '(' ), i ) >= 0 ) {
                        i2 = findCloser( tool );
                        flag = tool.slice( i + 1 , i2 ).trim();
                        rest = tool.slice( i2 + 1 ).trim();
                    }
                    else {
                        flag = null;
                        rest = '';
                    }
                    
                    // if toolName isn't natively supported, edit current selector and save tool for manual filter
                    if ( ( toolName !== 'not' || !notSupport( flag ) ) && resetregexp( exp ).exec( toolName ) ) {
                        
                        selector = before + rest + after;
                        r.push( { name : toolName , flag : flag } );
                        
                    }
                }
                
                return [ normalize( badEnd( selector ) ) || '*' , r.reverse() ];
            }
            
            return function ( selector , parent , fus ) {
                var i , l , i2 , l2 , child , splited;
                
                if ( compiler[ fus ] ) {
                    return compiler[ fus ];
                }
                
                splited = splitSelector( selector , parent );
                
                for ( i = 0, l = splited.length ; i < l ; i++ ) {
                    child = splited[ i ];
                    
                    for ( i2 = 0, l2 = child.length ; i2 < l2 ; i2++ ) {
                        child[ i2 ] = checkTool( beautify( child[ i2 ] ) );
                    }
                }
                
                return compiler( fus , splited ), splited;
            };
        } )();
        
        // call one after one all tool unsupported natively and redefined nodelist for each
        function advanceRequest ( object ) {
            for ( var i = 0 , t = object.tool , n = object.nodelist , l = t.length , _ ; i < l ; i++ ) {
                if ( !n || !n.length ) {
                    break;
                }
                
                _ = t[ i ];
                
                if ( advSelector[ _.name ] ) {
                    n = advSelector[ _.name ]( n , _.flag , object );
                }
            }
            return n || [];
        }
        
        function findChild ( nodelist , selector ) {
            for ( var i = 0 , l = nodelist.length , r = [] ; i < l ; ) {
                concat.apply( r , advanceRequest( {
                    nodelist : query( selector[ 0 ] , nodelist[ i ] ) ,
                    container : nodelist[ i++ ] ,
                    selector : selector[ 0 ] ,
                    tool : selector[ 1 ]
                } ) );
            }
            return r;
        }
        
        function deepfind ( firstTool , toolList , nodelist , container ) {
            var tmp = advanceRequest( {
                tool : firstTool[ 1 ] ,
                container : container ,
                selector : firstTool[ 0 ] ,
                nodelist : nodelist || query( firstTool[ 0 ] , container )
            } );
            
            for ( var i = 1 , l = toolList.length ; i < l ; ) {
                tmp = findChild( tmp , toolList[ i++ ] );
            }
            
            return tmp;
        }
        
        function browseCompiledList ( list , nodelist , container ) {
            for ( var i = 0 , r = [] , l = list.length , compiledList ; i < l ; i++ ) {
                compiledList = list[ i ];
                concat.apply( r , deepfind( compiledList[ 0 ] , compiledList , nodelist , container ) );
            }
            return r;
        }
        
        function find ( selector , container , parent ) {
            var r , f , p = parent || '*' , ss = selector + p;
            
            if ( ( f = findCache[ ss ] ) && f.c === container ) {
                return f.r;
            }
            
            level++;
            
            r = arrayunique( browseCompiledList( compiler( selector , p , ss ) , null , container ) );
            
            if ( --level > 0 ) {
                findCache( ss , { c : container , r : r } );
            }
            else {
                findCache = cacheHandler();
            }
            
            return r;
        }
        
        // targetJS
        var _T = function ( s , o ) {
            if ( isstring( s ) ) {
                return find( s , isnode( o ) ? o : document );
            }
            
            return _T.filter( s , o );
        };
        
        // filter nodelist
        _T.filter = function ( nodelist , filter , global ) {
            if ( isnodelist( nodelist ) || isarray( nodelist ) ) {
                nodelist = typedtoarray( nodelist );
                
                if ( isstring( filter ) ) {
                    if ( global ) {
                        return globalNodelistFilter( nodelist , filter );
                    }
                    
                    return nodelistFilter( nodelist , filter );
                }
                
                if ( isnode( filter ) ) {
                    return onlyChildsOf( filter , nodelist );
                }
                
                return nodelist;
            }
            
            return null;
        };
        
        // find nodes from nodelist
        _T.find = function ( nodelist , selector ) {
            var r = [] , compiled = compiler( selector , '*' , selector + '*' );
            
            for ( var i = 0 , l = nodelist.length , node ; i < l ; i++ ) {
                node = nodelist[ i ];
                concat.apply( r , browseCompiledList( compiled , null , node ) );
            }
            
            if ( level <= 0 ) {
                findCache = cacheHandler();
            }
            
            return arrayunique( r );
        };
        
        // find nodes from nodelist
        _T.apply = function ( nodelist , selector , container ) {
            var r = [] , compiled = compiler( selector , '*' , selector + '*' );
            
            concat.apply( r , browseCompiledList( compiled , nodelist , container || document ) );
            
            if ( level <= 0 ) {
                findCache = cacheHandler();
            }
            
            return arrayunique( r );
        };
        
        // find nodes from nodelist
        _T.applyEach = function ( nodelist , selector ) {
            var r = [] , compiled = compiler( selector , '*' , selector + '*' );
            
            for ( var i = 0 , l = nodelist.length , node ; i < l ; i++ ) {
                node = nodelist[ i ];
                concat.apply( r , browseCompiledList( compiled , [ node ] , node.parentNode ) );
            }
            
            if ( level <= 0 ) {
                findCache = cacheHandler();
            }
            
            return arrayunique( r );
        };
        
        // css match
        // use native function if it's possible
        _T.match = _tmatch = ( function () {
            function man ( e , s , b ) {
                return inarray( e , find( s , b || getdocument( e ) ) );
            }
            
            return function ( e , s , b ) {
                try {
                    return matchesSelector( e , s );
                } catch ( _ ) {
                    return man( e , s , b );
                }
            };
        } )();
        
        // HTMLElement.closest
        // use native function if it's possible
        _T.closest = _tclosest = ( function () {
            
            function man ( element , selector ) {
                
                if ( _tmatch( element , selector ) ) {
                    return element;
                }
                
                while ( isnode( element = element.parentNode ) ) {
                    if ( _tmatch( element , selector ) ) {
                        return element;
                    }
                }
                
                return null;
            }
            
            return function ( element , selector ) {
                try {
                    return closest( element , selector );
                } catch ( _ ) {
                    return man( element , selector );
                }
                
            };
        } )();
        
        /**
         * @public
         * @readonly
         * @function _T
         * */
        _defineProperty( global , '_T' , _T );
        
        /**
         * @public
         * @readonly
         * @function target
         * */
        _defineProperty( global , 'target' , _T );
        
        
    } )();
    
    /**
     * event-handler
     * @license MIT
     * @see {@link https://framagit.org/tla/event-handler}
     * */
    ( function () {
        
        var guid = 0 ,
            trimCache = cacheHandler( 1000 ) ,
            concat = Array.prototype.push;
        
        function createEventObject ( window , name ) {
            try {
                return new ( window.CustomEvent )( name , {
                    cancelable : true ,
                    bubbles : true
                } );
            } catch ( e ) {
                return null;
            }
        }
        
        var dispatch = ( function () {
            
            function dispatch ( node , event ) {
                if ( node.dispatchEvent ) {
                    node.dispatchEvent( event );
                }
            }
            
            return function ( node , eventName , data , emulate ) {
                var event;
                
                if ( eventName instanceof Event ) {
                    return dispatch( node , eventName );
                }
                
                // native event ( without data... )
                if ( ( !exist( data ) || !data.length ) && !emulate && isfunction( node[ eventName ] ) ) {
                    return node[ eventName ]();
                }
                
                // custom event
                if ( event = createEventObject( getwindow( node ) , eventName ) ) {
                    event.eventHandler = data || null;
                    event.manualyDispatched = true;
                    dispatch( node , event );
                }
            };
            
        } )();
        
        function commat_splitTrim ( ar ) {
            return splitTrim( ar , /,|\s/g );
        }
        
        function dot_splitTrim ( ar ) {
            return splitTrim( ar , '.' );
        }
        
        function parseNamespace ( str , def ) {
            if ( str = ( str || '' ).toString().trim() ) {
                str = dot_splitTrim( str );
                
                if ( str.length ) {
                    
                    if ( def && def.length ) {
                        concat.apply( str , def );
                    }
                    
                    return arrayunique( str ).sort();
                    
                }
            }
            
            return null;
        }
        
        function compareNamespace ( ar1 , ar2 ) {
            var check = false;
            
            if ( ar1 && ar1.length && ar2 && ar2.length ) {
                for ( var i = 0 , l = ar1.length ; i < l ; i++ ) {
                    if ( inarray( ar1[ i ] , ar2 ) ) {
                        check = true;
                        break;
                    }
                }
            }
            
            return check;
        }
        
        function parseEventsArguments ( event , defNamespace ) {
            var i;
            return ( i = event.indexOf( '.' ), i ) < 0 ? {
                    event : event || null ,
                    namespace : null
                } :
                {
                    event : event.slice( 0 , i ) || null ,
                    namespace : parseNamespace( event.slice( i + 1 ) , defNamespace )
                };
        }
        
        function buildEvents ( events , defNamespace ) {
            var t , e = events;
            
            if ( t = trimCache[ e ] ) {
                return t;
            }
            
            events = commat_splitTrim( events );
            
            for ( var i = 0 , r = [] , l = events.length ; i < l ; ) {
                r.push( parseEventsArguments( events[ i++ ] , defNamespace ) );
            }
            
            return trimCache( e , r ), r;
        }
        
        function sortNamespace ( events ) {
            var namespaces = {
                global : []
            };
            
            if ( events ) {
                events.forEach( function ( model ) {
                    if ( model.event ) {
                        if ( !namespaces[ model.event ] ) {
                            namespaces[ model.event ] = [];
                        }
                        
                        if ( model.namespace ) {
                            concat.apply( namespaces[ model.event ] , model.namespace );
                        }
                    }
                    else {
                        if ( model.namespace ) {
                            concat.apply( namespaces.global , model.namespace );
                        }
                    }
                } );
            }
            
            return function ( event ) {
                var r = namespaces.global.slice( 0 );
                
                if ( namespaces[ event ] ) {
                    concat.apply( r , namespaces[ event ] );
                }
                
                return r;
            };
        }
        
        var cloneSettings = ( function () {
            var prop = [ 'capture' , 'namespace' , 'once' , 'passive' , 'selector' , 'blacklist' ] , l = prop.length;
            
            return function ( origin ) {
                for ( var setting = {} , i = 0 , tmp ; i < l ; i++ ) {
                    tmp = prop[ i ];
                    setting[ tmp ] = origin[ tmp ];
                }
                return setting;
            };
        } )();
        
        var cloneEvent = ( function () {
            
            function setReadOnly ( obj , prop , val ) {
                Object.defineProperty( obj , prop , {
                    configurable : false ,
                    enumerable : true ,
                    writable : false ,
                    value : val
                } );
            }
            
            /** @typedef {!cevent#} EventClone */
            var cevent = function () {};
            
            [
                'altKey' , 'bubbles' , 'button' , 'buttons' ,
                'cancelable' , 'changedTouches' , 'clientX' , 'clientY' ,
                'clipboardData' , 'ctrlKey' , 'currentTarget' , 'composed' ,
                'data' , 'dataTransfer' , 'deltaX' , 'deltaY' , 'deltaZ' ,
                'deltaMode' , 'detail' , 'eventPhase' , 'fromElement' ,
                'getModifierState' , 'isComposing' , 'isTrusted' , 'key' ,
                'keyCode' , 'layerX' , 'layerY' , 'location' , 'metaKey' ,
                'movementX' , 'movementY' , 'offsetX' , 'offsetY' , 'pageX' ,
                'pageY' , 'relatedTarget' , 'region' , 'repeat' , 'screenX' ,
                'screenY' , 'shiftKey' , 'target' , 'targetTouches' , 'timeStamp' ,
                'toElement' , 'touches' , 'type' , 'view' , 'wheelDelta' ,
                'wheelDeltaX' , 'wheelDeltaY' , 'which' , 'x' , 'y' , 'manualyDispatched'
            ].forEach( function ( prop ) {
                Object.defineProperty( cevent.prototype , prop , {
                    get : function () {
                        return this.defaultEvent[ prop ];
                    }
                } );
            } );
            
            /**
             * @public
             * @readonly
             * @function mute
             * @memberof EventClone
             * */
            cevent.prototype.mute = function () {};
            
            /**
             * @public
             * @readonly
             * @function stopPropagation
             * @memberof EventClone
             * */
            cevent.prototype.stopPropagation = function () {
                this.defaultEvent.stopPropagation();
            };
            
            /**
             * @public
             * @readonly
             * @function stopImmediatePropagation
             * @memberof EventClone
             * */
            cevent.prototype.stopImmediatePropagation = function () {
                this.defaultEvent.stopImmediatePropagation();
            };
            
            /**
             * @public
             * @readonly
             * @function preventDefault
             * @memberof EventClone
             * */
            cevent.prototype.preventDefault = function () {
                if ( this.defaultEvent.cancelable && !this.defaultPrevented ) {
                    this.defaultEvent.preventDefault();
                }
            };
            
            /**
             * @public
             * @readonly
             * @function getPreventDefault
             * @memberof EventClone
             * */
            cevent.prototype.getPreventDefault = function () {
                return this.defaultPrevented;
            };
            
            /**
             * @public
             * @readonly
             * @function isDefaultPrevented
             * @memberof EventClone
             * */
            cevent.prototype.isDefaultPrevented = function () {
                return this.defaultPrevented;
            };
            
            /**
             * @public
             * @readonly
             * @property {boolean} defaultPrevented
             * @memberof EventClone
             * */
            Object.defineProperty( cevent.prototype , 'defaultPrevented' , {
                get : function () {
                    var e = this.defaultEvent;
                    
                    return isset( e.defaultPrevented ) ? e.defaultPrevented :
                        isset( e.getPreventDefault ) ? e.getPreventDefault() : false;
                }
            } );
            
            /**
             * @function cloneEvent
             * @return {cevent}
             * */
            return function ( event , setting , closest , element , override ) {
                
                /** @type {EventClone} */
                var a = new cevent();
                
                /**
                 * @public
                 * @readonly
                 * @property {Object} setting
                 * @memberof EventClone
                 * */
                setReadOnly( a , 'setting' , setting );
                
                /**
                 * @public
                 * @readonly
                 * @property {?(HTMLElement|Element|Node)} closest
                 * @memberof EventClone
                 * */
                setReadOnly( a , 'closest' , closest );
                
                /**
                 * @public
                 * @readonly
                 * @property {HTMLElement|Element|Node} element
                 * @memberof EventClone
                 * */
                setReadOnly( a , 'element' , element );
                
                /**
                 * @public
                 * @readonly
                 * @property {event} defaultEvent
                 * @memberof EventClone
                 * */
                setReadOnly( a , 'defaultEvent' , event );
                
                if ( isobject( override ) ) {
                    for ( var i in override ) {
                        setReadOnly( a , i , override[ i ] );
                    }
                }
                
                if ( setting.passive && !eventPassiveSupport ) {
                    
                    /**
                     * @public
                     * @readonly
                     * @function preventDefault
                     * @memberof EventClone
                     * */
                    setReadOnly( a , 'preventDefault' , function () {
                        throw "Unable to preventDefault inside passive event listener invocation.";
                    } );
                }
                
                return a;
            };
            
        } )();
        
        /** @class eventHandler */
        function eventHandler () {
            var self = this;
            
            /**
             * @public
             * @property {number} listened
             * @memberof eventHandler#
             * */
            this.listened = 0;
            
            /**
             * @public
             * @function closest
             * @memberof eventHandler#
             * */
            this.closest = closest;
            
            /**
             * @private
             * @property {number} guid
             * @memberof eventHandler#
             * */
            this.guid = 0;
            
            /**
             * @private
             * @property {document} document
             * @memberof eventHandler#
             * */
            this.document = document;
            
            /**
             * @private
             * @property {boolean} lightMemory
             * @memberof eventHandler#
             * */
            this.lightMemory = false;
            
            /**
             * @private
             * @property {?function} selfDestruction
             * @memberof eventHandler#
             * */
            this.selfDestruction = null;
            
            /**
             * @private
             * @property {boolean} selfDestructionEnabled
             * @memberof eventHandler#
             * */
            this.selfDestructionEnabled = false;
            
            /**
             * @private
             * @property {?number} selfDestructionTimeout
             * @memberof eventHandler#
             * */
            this.selfDestructionTimeout = null;
            
            /**
             * @private
             * @property {?number} cleanupInterval
             * @memberof eventHandler#
             * */
            this.cleanupInterval = null;
            
            /**
             * @private
             * @property {boolean} defaultPassive
             * @memberof eventHandler#
             * */
            this.defaultPassive = false;
            
            /**
             * @private
             * @property {Object} indexer
             * @memberof eventHandler#
             * */
            this.indexer = {};
            
            /**
             * @private
             * @property {Array} nodelist
             * @memberof eventHandler#
             * */
            this.nodelist = [];
            
            /**
             * @private
             * @property {?MutationObserver} observer
             * @memberof eventHandler#
             * */
            this.observer = null;
            
            /**
             * @private
             * @property {?number} observerTimeout
             * @memberof eventHandler#
             * */
            this.observerTimeout = null;
            
            /**
             * @private
             * @property {string} token
             * @memberof eventHandler#
             * */
            this.token = 'event-handler-guid-' + ( ++guid );
            
            /**
             * @private
             * @function handleEvent
             * @memberof eventHandler#
             * */
            this.handleEvent = function ( e ) {
                var i = 0 ,
                    arg = [] ,
                    remove = [] ,
                    namespace = null ,
                    token = self.token ,
                    element = this.element ,
                    closestElement , event , filters , clone , tmp , brk = false;
                
                if ( e.eventHandler ) {
                    if ( token !== e.eventHandler.token ) {
                        return;
                    }
                    namespace = e.eventHandler.namespace;
                    arg = e.eventHandler.arg;
                }
                
                for ( var callbacklist = this.callbacks.slice( 0 ) , l = callbacklist.length , data ; i < l ; i++ ) {
                    if ( brk ) {
                        break;
                    }
                    
                    data = callbacklist[ i ];
                    
                    // -----
                    
                    // check namespace match / blacklist and selector filter
                    
                    filters = {
                        
                        get namespace () {
                            return !namespace || compareNamespace( namespace , data.namespace );
                        } ,
                        
                        get blacklist () {
                            return !data.blacklist || !self.closest( e.target , data.blacklist );
                        } ,
                        
                        get selector () {
                            return !data.selector || ( closestElement = self.closest( e.target , data.selector ) );
                        }
                        
                    };
                    
                    // -----
                    
                    if ( filters.selector && filters.namespace && filters.blacklist ) {
                        
                        clone = cloneSettings( data );
                        
                        // check autokill
                        if ( isfunction( data.autokill ) && data.autokill( clone ) === true ) {
                            remove.push( data );
                            continue;
                        }
                        
                        // clone event
                        /** @type {EventClone} */
                        event = cloneEvent( e , clone , closestElement || null , element , {
                            
                            /**
                             * @public
                             * @override
                             * @function stopImmediatePropagation
                             * @memberof event#
                             * */
                            stopImmediatePropagation : function () {
                                this.defaultEvent.stopImmediatePropagation();
                                brk = true;
                            } ,
                            
                            /**
                             * @public
                             * @override
                             * @function mute
                             * @memberof event#
                             * */
                            mute : function () {
                                !data.once && remove.push( data );
                            }
                            
                        } );
                        
                        // check once
                        if ( data.once ) {
                            remove.push( data );
                        }
                        
                        // callback
                        concat.apply( ( tmp = [ event ] , tmp ) , arg );
                        data.caller.apply( closestElement || element , tmp );
                    }
                }
                
                // autokill / once / mute purge
                // -------------------------------------------------
                remove.length && self.muteCallbacks( remove );
            };
        }
        
        /**
         * @public
         * @function setDocument
         * @memberof eventHandler#
         * */
        eventHandler.prototype.setDocument = function ( d ) {
            this.document = d;
            this.setLightMemoryMode( this.lightMemory );
        };
        
        /**
         * @private
         * @function muteCallbacks
         * @memberof eventHandler#
         * */
        eventHandler.prototype.muteCallbacks = function ( callbacks ) {
            var self = this ,
                handler , handlers ,
                element , callList , index;
            
            foreach( toarray( callbacks ) , function ( callback ) {
                handler = callback.referer;
                handlers = handler.referer;
                element = handler.element;
                callList = handler.callbacks;
                
                if ( callList.length ) {
                    index = callList.indexOf( callback );
                    
                    if ( index > -1 ) {
                        callList.splice( callList.indexOf( callback ) , 1 );
                        self.listened--;
                    }
                }
                
                if ( !callList.length ) {
                    element.removeEventListener( handler.event , handler , handler.capture );
                    delete handlers[ handler.name ];
                }
                
                if ( isEmptyObject( handlers ) ) {
                    self.deleteElement( element );
                }
            } );
        };
        
        /**
         * @private
         * @function cleanup
         * @memberof eventHandler#
         * */
        eventHandler.prototype.cleanup = ( function () {
            var remove = [];
            
            function browseCallbacks ( handleObject ) {
                var callbacks = handleObject.callbacks;
                
                for ( var i = 0 , l = callbacks.length , callback ; i < l ; i++ ) {
                    callback = callbacks[ i ];
                    if ( callback.autokill && callback.autokill( handleObject ) === true ) {
                        remove.push( callback );
                    }
                }
            }
            
            function browsehandlers ( handler ) {
                for ( var h in handler ) {
                    browseCallbacks( handler[ h ] );
                }
                
                if ( remove.length ) {
                    this.muteCallbacks( remove );
                }
                
                remove = [];
            }
            
            return function () {
                var i , x , tmp;
                
                if ( isEmptyObject( this.indexer ) ) {
                    return this.checkSelfDestruction();
                }
                
                for ( i in x = this.indexer ) {
                    tmp = x[ i ];
                    browsehandlers.call( this , tmp.handlers );
                }
            };
        } )();
        
        /**
         * @public
         * @function setCleanupInterval
         * @memberof eventHandler#
         * */
        eventHandler.prototype.setCleanupInterval = function ( n ) {
            var self = this;
            clearInterval( this.cleanupInterval );
            if ( exist( n ) && Number.isInteger( n ) ) {
                this.cleanupInterval = setInterval( function () { self.cleanup.call( self ); } , n );
            }
        };
        
        /**
         * @public
         * @function setDefaultPassive
         * @memberof eventHandler#
         * */
        eventHandler.prototype.setDefaultPassive = function ( b ) {
            this.defaultPassive = !!b;
        };
        
        /**
         * @public
         * @function setLightMemoryMode
         * @memberof eventHandler#
         * */
        eventHandler.prototype.setLightMemoryMode = function ( b ) {
            this.lightMemory = !!b;
            
            this.disableObserver();
            
            if ( this.lightMemory ) {
                this.enableObserver();
            }
        };
        
        /**
         * @public
         * @function setSelfDestruction
         * @memberof eventHandler#
         * */
        eventHandler.prototype.setSelfDestruction = function ( fn ) {
            this.selfDestruction = isfunction( fn ) ? fn : null;
            this.selfDestructionEnabled = !!this.selfDestruction;
            this.checkSelfDestruction();
        };
        
        // -----------------------------------
        // -----------------------------------
        
        /**
         * @private
         * @function setPassive
         * @memberof eventHandler#
         * */
        eventHandler.prototype.setPassive = function ( b ) {
            return ( exist( b ) && typeof ( b ) == 'boolean' ) ? b : this.defaultPassive;
        };
        
        /**
         * @private
         * @function observerCallback
         * @memberof eventHandler#
         * */
        eventHandler.prototype.observerCallback = function () {
            clearTimeout( this.observerTimeout );
            this.observerTimeout = setTimeout( function () {
                var d = this.document;
                
                for ( var a = this.nodelist , i = 0 , t ; i < a.length ; i++ ) {
                    t = a[ i ];
                    
                    if ( t.nodeType != 1 ) {
                        continue;
                    }
                    
                    if ( !d.contains( t ) ) {
                        this.mute( t );
                        i--;
                    }
                }
                
                if ( !this.nodelist.length ) {
                    this.disableObserver();
                }
            }.bind( this ) , 100 );
        };
        
        /**
         * @private
         * @function enableObserver
         * @memberof eventHandler#
         * */
        eventHandler.prototype.enableObserver = function () {
            if ( !this.lightMemory || this.observer ) {
                return;
            }
            
            var mutation = global.MutationObserver || global.WebKitMutationObserver || null;
            
            if ( mutation ) {
                this.observer = new mutation( this.observerCallback.bind( this ) );
                this.observer.observe( this.document , { childList : true , subtree : true } );
                this.observerCallback();
            }
        };
        
        /**
         * @private
         * @function disableObserver
         * @memberof eventHandler#
         * */
        eventHandler.prototype.disableObserver = function () {
            clearTimeout( this.observerTimeout );
            if ( this.observer ) {
                this.observer.disconnect();
                this.observer = null;
            }
        };
        
        /**
         * @private
         * @function spliceNodelist
         * @memberof eventHandler#
         * */
        eventHandler.prototype.spliceNodelist = function ( e ) {
            var i = this.nodelist.indexOf( e );
            i >= 0 && this.nodelist.splice( i , 1 );
        };
        
        /**
         * @private
         * @function checkSelfDestruction
         * @memberof eventHandler#
         * */
        eventHandler.prototype.checkSelfDestruction = function () {
            clearTimeout( this.selfDestructionTimeout );
            if ( this.selfDestructionEnabled ) {
                this.selfDestructionTimeout = setTimeout( function () {
                    if ( !this.nodelist.length && isfunction( this.selfDestruction ) && this.selfDestruction( this ) === true ) {
                        this.destroy();
                    }
                }.bind( this ) , 100 );
            }
        };
        
        /**
         * @private
         * @function parseObject
         * @memberof eventHandler#
         * */
        eventHandler.prototype.parseObject = function ( element , json , selector , capture ) {
            var i;
            
            if ( !selector ) {
                for ( i in json ) {
                    this.listen( element , i , json[ i ] , capture );
                }
            }
            else {
                for ( i in json ) {
                    this.listen( element , i , selector , json[ i ] , capture );
                }
            }
        };
        
        /**
         * @private
         * @function validObjectArgument
         * @memberof eventHandler#
         * */
        eventHandler.prototype.validObjectArgument = function ( args , object ) {
            isboolean( object.once ) && ( args.once = !!object.once );
            isboolean( object.capture ) && ( args.capture = !!object.capture );
            args.passive = this.setPassive( object.passive );
            
            isstring( object.selector ) && ( args.selector = object.selector );
            isstring( object.blacklist ) && ( args.blacklist = object.blacklist );
            
            args.autokill = isfunction( object.autokill ) && object.autokill || null;
            args.namespace = parseNamespace( object.namespace );
        };
        
        /**
         * @private
         * @function parseArguments
         * @memberof eventHandler#
         * */
        eventHandler.prototype.parseArguments = function ( element , event , selector , callback , capture ) {
            
            if ( isobject( event ) ) {
                return this.parseObject( element , event , null , selector );
            }
            
            if ( isobject( selector ) ) {
                return this.parseObject( element , selector , event , callback );
            }
            
            if ( !element[ this.token ] ) {
                element[ this.token ] = ++this.guid;
                
                this.nodelist.push( element );
                
                this.indexer[ element[ this.token ] ] = {
                    element : element ,
                    handlers : {}
                };
            }
            
            var args = {
                callback : callback ,
                
                passive : this.defaultPassive ,
                capture : isboolean( capture ) && capture ,
                once : false ,
                
                selector : selector ,
                blacklist : null ,
                
                namespace : null ,
                autokill : null
            };
            
            if ( isfunction( selector ) ) {
                args.capture = isboolean( callback ) && !!callback;
                args.callback = selector;
                args.selector = null;
                
                if ( isobject( callback ) ) {
                    this.validObjectArgument( args , callback );
                }
            }
            
            if ( isobject( capture ) ) {
                this.validObjectArgument( args , capture );
            }
            
            args.events = buildEvents( event , args.namespace );
            
            if ( !args.callback[ this.token ] ) {
                args.callback[ this.token ] = ++this.guid;
            }
            
            return args;
        };
        
        /**
         * @private
         * @function findHandlers
         * @memberof eventHandler#
         * */
        eventHandler.prototype.findHandlers = function ( element , events , checkNamespace ) {
            var self = this ,
                eventsList , _token;
            
            function createEventsList () {
                eventsList = [];
                
                ( events = ( isarray( events ) ? events : buildEvents( events ) ) ).forEach( function ( model ) {
                    if ( model.event ) {
                        eventsList.push( model.event );
                    }
                } );
            }
            
            function loop ( condition ) {
                var r = [] , index = {} ,
                    element , token , rih , rc , ro;
                
                forin( self.indexer , function ( key , indexer ) {
                    element = indexer.element;
                    token = element[ self.token ];
                    
                    if ( token ) {
                        forin( indexer.handlers , function ( key , handler ) {
                            
                            if ( condition( token , handler.event ) ) {
                                
                                if ( !index[ token ] ) {
                                    ro = {
                                        element : element ,
                                        handlers : {} ,
                                        callbacks : []
                                    };
                                    
                                    index[ token ] = r.push( ro );
                                    
                                    rih = ro.handlers;
                                    rc = ro.callbacks;
                                }
                                
                                rih[ key ] = handler;
                                
                                if ( checkNamespace ) {
                                    concat.apply( rc , filterByNamespace( handler.event , handler.callbacks ) );
                                }
                            }
                            
                        } );
                    }
                } );
                
                return r;
            }
            
            function filterByNamespace ( event , callbacks ) {
                var r = [];
                
                foreach( events , function ( model ) {
                    if ( !model.event || model.event == event ) {
                        for ( var i = 0 , l = callbacks.length , tmp ; i < l ; i++ ) {
                            tmp = callbacks[ i ];
                            
                            if ( !model.namespace || !tmp.namespace || compareNamespace( model.namespace , tmp.namespace ) ) {
                                r.push( tmp );
                            }
                        }
                    }
                } );
                
                return r;
            }
            
            if ( element && events ) {
                if ( !( _token = element[ self.token ] ) ) {
                    return [];
                }
                
                createEventsList();
                
                return loop( function ( token , event ) {
                    return _token == token && ( !eventsList.length || inarray( event , eventsList ) );
                } );
            }
            
            if ( events ) {
                createEventsList();
                
                return loop( function ( token , event ) {
                    return !eventsList.length || inarray( event , eventsList );
                } );
            }
            
            if ( element ) {
                if ( !( _token = element[ self.token ] ) ) {
                    return [];
                }
                
                return ( function () {
                    var r = [] ,
                        handlers , element , token;
                    
                    forin( self.indexer , function ( key , indexer ) {
                        element = indexer.element;
                        handlers = indexer.handlers;
                        token = element[ self.token ];
                        
                        if ( token && _token == token ) {
                            r.push( {
                                element : element ,
                                handlers : handlers
                            } );
                        }
                    } );
                    
                    return r;
                } )();
            }
            
            return [];
        };
        
        /**
         * @public
         * @function listen
         * @memberof eventHandler#
         * */
        eventHandler.prototype.listen = function ( element , events , selector , callback , capture ) {
            var data , arg , tmp , eventName , nameKey , handler;
            
            if ( !( arg = this.parseArguments( element , events , selector , callback , capture ) ) ) {
                return;
            }
            
            data = this.indexer[ element[ this.token ] ];
            
            nameKey = ( +arg.capture ) /*+ (+arg.passive)*/;
            
            events = arg.events;
            
            for ( var i = 0 , l = events.length ; i < l ; ) {
                tmp = events[ i++ ];
                eventName = tmp.event + nameKey;
                handler = data.handlers[ eventName ];
                
                if ( !handler ) {
                    element.addEventListener(
                        tmp.event ,
                        
                        data.handlers[ eventName ] = handler = {
                            name : eventName ,
                            callbacks : [] ,
                            element : element ,
                            event : tmp.event ,
                            capture : arg.capture ,
                            referer : data.handlers ,
                            handleEvent : this.handleEvent
                        } ,
                        
                        arg.passive && eventPassiveSupport ? {
                                capture : arg.capture ,
                                passive : arg.passive
                            } :
                            arg.capture
                    );
                }
                
                handler.callbacks.push( {
                    referer : handler ,
                    
                    caller : arg.callback ,
                    guid : arg.callback[ this.token ] ,
                    
                    once : arg.once ,
                    passive : arg.passive ,
                    capture : arg.capture ,
                    
                    selector : arg.selector ,
                    blacklist : arg.blacklist ,
                    
                    autokill : arg.autokill ,
                    namespace : tmp.namespace
                } );
                
                this.listened++;
            }
            
            this.enableObserver();
        };
        
        /**
         * @public
         * @function localDispatch
         * @memberof eventHandler#
         * */
        eventHandler.prototype.localDispatch = function ( element , events , arg ) {
            var self = this;
            
            function ready ( result ) {
                var remove = [] ,
                    getNamespaceArgument , element ,
                    namespaces , fakeevent , clone , fakeclone , tmp;
                
                foreach( result , function ( data ) {
                    element = data.element;
                    getNamespaceArgument = sortNamespace( events );
                    
                    // for each callback
                    foreach( data.callbacks , function ( callback ) {
                        
                        // check namespace
                        if ( events ) {
                            namespaces = getNamespaceArgument( callback.referer.event );
                            
                            if ( namespaces.length && !compareNamespace( namespaces , callback.namespace ) ) {
                                return;
                            }
                        }
                        
                        clone = cloneSettings( callback );
                        
                        // check autokill
                        if ( isfunction( callback.autokill ) && callback.autokill( clone ) === true ) {
                            return remove.push( callback );
                        }
                        
                        fakeevent = createEventObject( getdocument( element ) , callback.referer.event );
                        
                        /** @type {EventClone} */
                        fakeclone = cloneEvent( fakeevent , cloneSettings( callback ) , null , element , {
                            originalTarget : element ,
                            currentTarget : element ,
                            srcElement : element ,
                            target : element ,
                            
                            /**
                             * @public
                             * @override
                             * @function mute
                             * @memberof fakeclone#
                             * */
                            mute : function () {
                                !callback.once && remove.push( callback );
                            }
                        } );
                        
                        // check once
                        if ( callback.once ) {
                            remove.push( callback );
                        }
                        
                        concat.apply( ( tmp = [ fakeclone ] , tmp ) , toarray( arg ) );
                        
                        callback.caller.apply( element , tmp );
                    } );
                } );
                
                // autokill / once / mute purge
                if ( remove.length ) {
                    self.muteCallbacks( remove );
                }
            }
            
            var result = ( function () {
                
                if ( exist( element ) && isstring( events ) ) {
                    events = buildEvents( events );
                    return self.findHandlers( element , events , true );
                }
                
                if ( isstring( element ) ) {
                    arg = events;
                    events = buildEvents( element );
                    return self.findHandlers( null , events , true );
                }
                
                if ( exist( element ) ) {
                    arg = events;
                    events = null;
                    return self.findHandlers( element , null );
                }
                
            } )();
            
            if ( result && result.length ) {
                return ready( result );
            }
            
        };
        
        /**
         * @public
         * @function dispatch
         * @memberof eventHandler#
         * */
        eventHandler.prototype.dispatch = function ( element , events , arg , forceEmulate ) {
            var emulate = false , self = this;
            
            if ( exist( element ) && isstring( events ) ) {
                arg = toarray( arg );
                events = buildEvents( events );
                emulate = !!( arg.length || forceEmulate );
                
                events.forEach( function ( model ) {
                    if ( model.event ) {
                        dispatch( element , model.event , {
                            namespace : model.namespace ,
                            token : self.token ,
                            arg : arg
                        } , emulate );
                    }
                } );
            }
            
        };
        
        /**
         * @public
         * @function mute
         * @memberof eventHandler#
         * */
        eventHandler.prototype.mute = function ( element , events , fn ) {
            var guid , self = this;
            
            // mute all
            if ( !exist( element ) && !exist( events ) ) {
                return this.bigCrunch();
            }
            
            if ( isfunction( events ) ) {
                fn = events;
            }
            
            // invalid function
            if ( isfunction( fn ) && !( guid = fn[ this.token ] ) ) {
                return;
            }
            
            function ready ( result ) {
                var callbacks , callback ,
                    namespaces , element ,
                    getNamespaceArgument = sortNamespace( events );
                
                // browse all handleEvent that match
                foreach( result , function ( data ) {
                    element = data.element;
                    
                    // for each handlers
                    forin( data.handlers , function ( key , handler ) {
                        
                        // filter by namespace or function
                        if ( events || guid ) {
                            callbacks = handler.callbacks;
                            
                            for ( var i = 0 , l = callbacks.length ; i < l ; i++ ) {
                                callback = callbacks[ i ];
                                
                                // filter by namespace
                                if ( events ) {
                                    namespaces = getNamespaceArgument( handler.event );
                                    
                                    if ( namespaces.length && !compareNamespace( namespaces , callback.namespace ) ) {
                                        continue;
                                    }
                                }
                                
                                // filter by function
                                if ( guid && callback.guid !== guid ) {
                                    continue;
                                }
                                
                                callbacks.splice( i , 1 );
                                self.listened--;
                                i--;
                                l--;
                            }
                        }
                        // or just filter by event
                        else {
                            self.listened -= handler.callbacks.length;
                            handler.callbacks = [];
                        }
                        
                        // if haven't any callback, remove the event and delete the object
                        if ( !handler.callbacks.length ) {
                            element.removeEventListener( handler.event , handler , handler.capture );
                            delete handler.referer[ handler.name ];
                        }
                    } );
                    
                    // if havn't any events for the element, purge it
                    if ( isEmptyObject( self.indexer[ element[ self.token ] ].handlers ) ) {
                        self.deleteElement( data.element );
                    }
                    
                } );
                
            }
            
            var result = ( function () {
                
                if ( exist( element ) && isstring( events ) ) {
                    events = buildEvents( events );
                    return self.findHandlers( element , events );
                }
                
                if ( isstring( element ) ) {
                    events = buildEvents( element );
                    return self.findHandlers( null , events );
                }
                
                if ( exist( element ) ) {
                    events = null;
                    return self.findHandlers( element , null );
                }
                
            } )();
            
            if ( result && result.length ) {
                return ready( result );
            }
            
        };
        
        /**
         * @public
         * @function muteChild
         * @memberof eventHandler#
         * */
        eventHandler.prototype.muteChild = function ( parent , event , callback ) {
            this.mute( parent , event , callback );
            
            for ( var a = getallchildren( parent ) , i = 0 , l = a.length ; i < l ; i++ ) {
                this.mute( a[ i ] , event , callback );
            }
        };
        
        /**
         * @private
         * @function deleteElement
         * @memberof eventHandler#
         * */
        eventHandler.prototype.deleteElement = function ( e ) {
            delete this.indexer[ e[ this.token ] ];
            delete e[ this.token ];
            this.spliceNodelist( e );
            this.checkSelfDestruction();
        };
        
        /**
         * @private
         * @function bigCrunch
         * @memberof eventHandler#
         * */
        eventHandler.prototype.bigCrunch = function () {
            var self = this , handler , i;
            
            forin( this.indexer , function ( key , tmp ) {
                for ( i in tmp.handlers ) {
                    handler = tmp.handlers[ i ];
                    tmp.element.removeEventListener( handler.event , handler , handler.capture );
                    self.listened -= handler.callbacks.length;
                }
                
                delete tmp.element[ self.token ];
            } );
            
            this.guid = 0;
            this.indexer = {};
            this.nodelist = [];
            return this.checkSelfDestruction();
        };
        
        /**
         * @public
         * @function destroy
         * @memberof eventHandler#
         * */
        eventHandler.prototype.destroy = function () {
            clearInterval( this.cleanupInterval );
            delete this.cleanupInterval;
            
            clearTimeout( this.selfDestructionTimeout );
            delete this.selfDestructionEnabled;
            delete this.selfDestructionTimeout;
            delete this.selfDestruction;
            
            this.disableObserver();
            delete this.observerTimeout;
            delete this.lightMemory;
            delete this.observer;
            
            this.bigCrunch();
            
            delete this.defaultPassive;
            delete this.handleEvent;
            delete this.document;
            delete this.nodelist;
            delete this.indexer;
            delete this.token;
            delete this.guid;
        };
        
        /**
         * @public
         * @property {Array} eventHandler.shortcuts
         * */
        ( eventHandler.shortcuts = [
            'click' , 'dblclick' , 'contextmenu' ,
            'mouseup' , 'mousedown' , 'mousemove' , 'mouseover' , 'mouseout' ,
            'blur' , 'focus' , 'change' , 'input' , 'submit' ,
            'keydown' , 'keypress' , 'keyup' ,
            'touchstart' , 'touchmove' , 'touchend' , 'touchcancel' ,
            'dragstart' , 'dragenter' , 'dragover' , 'dragleave' , 'drop' ,
            'error' , 'scroll' , 'load' , 'resize'
        ] ).forEach( function ( name ) {
            eventHandler.prototype[ name ] = function ( element , selector , callback , capture ) {
                if ( arguments.length <= 3 && !isfunction( selector ) && !isfunction( callback ) ) {
                    return this.dispatch( element , name , selector , callback );
                }
                
                this.listen( element , name , selector , callback , capture );
            };
        } );
        
        /**
         * @public
         * @function eventHandler.redirect
         * */
        eventHandler.redirect = function ( element , e ) {
            var oldData;
            
            e.stopImmediatePropagation();
            
            if ( e.defaultEvent ) {
                e = e.defaultEvent;
            }
            
            if ( e.originalEvent ) {
                e = e.originalEvent;
            }
            
            oldData = e.eventHandler || null;
            
            e = new e.constructor( e.type , e );
            
            if ( oldData ) {
                e.eventHandler = oldData;
            }
            
            dispatch( element , e );
        };
        
        /**
         * @public
         * @readonly
         * @function eventHandler
         * @return eventHandler#
         * */
        _defineProperty( global , 'eventHandler' , eventHandler );
        
    } )();
    
    /**
     * css3-animation
     * @license MIT
     * @see {@link https://framagit.org/tla/css3-animation}
     * */
    ( function () {
        
        var fingerprintCache = cacheHandler() ,
            
            guid = 'guid-animation-' + randomstring() ,
            
            patternTime = {
                slow : 850 ,
                fast : 250 ,
                origin : 400
            } ,
            
            // save display property
            sdp = ( function () {
                var data = {};
                
                return {
                    
                    get : function ( element ) {
                        return data[ element[ guid ] ] || null;
                    } ,
                    
                    set : function ( element ) {
                        data[ element[ guid ] ] = element.style.getPropertyValue( 'display' );
                    }
                    
                };
            } )() ,
            
            stylesSnapshot = ( function () {
                var cssOrigin = {} ,
                    cssToRemove = {};
                
                return {
                    
                    make : function ( element , properties ) {
                        var i = 0 ,
                            origin = {} ,
                            toRemove = [] ,
                            l = properties.length ,
                            id = element[ guid ] ,
                            manual = parseCss( element.style.cssText );
                        
                        for ( ; i < l ; i++ ) {
                            if ( !!manual[ properties[ i ] ] ) {
                                origin[ properties[ i ] ] = manual[ properties[ i ] ];
                            }
                            else {
                                toRemove.push( properties[ i ] );
                            }
                        }
                        
                        cssOrigin[ id ] = cssOrigin[ id ] ? overrideObject( cssOrigin[ id ] , origin ) : origin;
                        cssToRemove[ id ] = cssToRemove[ id ] ? overrideObject( cssToRemove[ id ] , toRemove ) : toRemove;
                    } ,
                    
                    recover : function ( element ) {
                        var i = 0 ,
                            id = element[ guid ] ,
                            origin = cssOrigin[ id ] ,
                            toRemove = cssToRemove[ id ] ,
                            l = toRemove.length;
                        
                        for ( ; i < l ; i++ ) {
                            element.style.removeProperty( toRemove[ i ] );
                        }
                        
                        forin( origin , function ( key , value ) {
                            element.style.setProperty( key , value.val , value.imp );
                        } );
                        
                        delete cssOrigin[ id ];
                        delete cssToRemove[ id ];
                    } ,
                    
                    erase : function ( element ) {
                        var id;
                        
                        if ( id = element[ guid ] ) {
                            delete cssOrigin[ id ];
                            delete cssToRemove[ id ];
                        }
                    }
                    
                };
            } )();
        
        function setCss ( node , json ) {
            forin( json , function ( key , value ) {
                node.style.setProperty( key , value );
            } );
        }
        
        function hackAuto ( v ) {
            return v.trim().toLowerCase() === 'auto' ? null : v;
        }
        
        function fingerprint ( c , m ) {
            var s = JSON.stringify( c ) , r , i , l , t;
            
            if ( t = fingerprintCache[ ' ' + s ] ) {
                return t;
            }
            
            for ( r = 0, i = 0, l = s.length ; i < l ; ) {
                r = ( ( r << 5 ) - r ) + s.charCodeAt( i++ );
                r = r & r;
            }
            
            t = parseInt( ( r + s.length ).toString() + m );
            fingerprintCache( s , ' ' + t );
            
            return t;
        }
        
        function parseCss ( cssText ) {
            var split = cssText.split( ';' ) ,
                l = split.length ,
                css = {} ,
                i = 0 ,
                tmp , value;
            
            for ( ; i < l ; ) {
                if ( tmp = split[ i++ ].trim() ) {
                    tmp = tmp.split( ':' ), value = tmp[ 1 ].trim();
                    
                    css[ tmp[ 0 ].trim() ] = !( tmp = /(!important)$/gi.exec( value ) ) ?
                        {
                            val : value ,
                            imp : ""
                        } :
                        {
                            val : value.substring( 0 , tmp.index ).trim() ,
                            imp : "important"
                        };
                }
            }
            
            return css;
        }
        
        function dataAnim ( c , d ) {
            return [
                isfunction( c ) && c || isfunction( d ) && d || null ,
                ( isset( c ) && isinteger( c ) ) && +c || patternTime[ c ] || patternTime.origin
            ];
        }
        
        function build ( document , global ) {
            
            function is_hidden ( element ) {
                return ( getstyleproperty( element , 'opacity' ) == '0' ||
                    getstyleproperty( element , 'display' ) === 'none' ||
                    getstyleproperty( element , 'visibility' ) === 'hidden' );
            }
            
            function getEmulateDisplayProp ( tag ) {
                var fragment = document.createDocumentFragment() ,
                    node = document.createElement( tag.toUpperCase() ) ,
                    c;
                
                fragment.appendChild( node );
                
                c = getstyleproperty( node , 'display' );
                
                if ( c == 'none' ) {
                    c = '';
                }
                
                return fragment = node = null, c;
            }
            
            function getVisibleDisplayValue ( node ) {
                var prop ,
                    current = save( node , [ 'display' ] ) ,
                    initial = function () {
                        if ( current.display.computed != 'none' ) {
                            return '';
                        }
                        
                        return getEmulateDisplayProp( node.tagName ) || 'block';
                    };
                
                prop = sdp.get( node );
                
                if ( isstring( prop ) && prop == 'none' ) {
                    return initial();
                }
                
                prop = current.display.inline;
                
                if ( !isstring( prop ) || prop == 'none' ) {
                    return initial();
                }
                
                return prop;
            }
            
            function calcProp ( element , prop ) {
                var tmp = element.cloneNode( true ) , data , _prop;
                
                _prop = prop.slice( 0 , 1 ).toUpperCase() + prop.slice( 1 ).toLowerCase();
                
                tmp.style.cssText = element.style.cssText;
                
                setCss( tmp , {
                    "position" : "absolute" ,
                    "visibility" : "hidden" ,
                    "display" : "block"
                } );
                
                element.parentNode.appendChild( tmp );
                data = hackAuto( getstyleproperty( tmp , prop ) ) || ( tmp[ 'client' + _prop ] || tmp[ 'offset' + _prop ] ) + 'px';
                
                return tmp.parentNode.removeChild( tmp ), data;
            }
            
            function callEnd ( done , node , p ) {
                if ( done ) {
                    if ( Array.isArray( done ) ) {
                        done.forEach( function ( fn ) {
                            fn.call( node , p );
                        } );
                    }
                    else {
                        done.call( node , p );
                    }
                }
            }
            
            function getRightOpacity ( data ) {
                var properties = [ 'inline' , 'computed' ];
                
                for ( var i = 0 , l = properties.length , property ; i < l ; i++ ) {
                    property = properties[ i ];
                    
                    if ( exist( data[ property ] ) && data[ property ] != 0 ) {
                        return data[ property ];
                    }
                }
                
                return '1';
            }
            
            function downQueue ( element ) {
                var setting , data = _anime.element[ element[ guid ] ];
                
                // continue animation if need, else state = false
                if ( data[ 'queue' ].length <= 0 ) {
                    delete _anime.element[ element[ guid ] ];
                    delete element[ guid ];
                    data = null;
                }
                else {
                    // get arguments of next animation
                    setting = data[ 'queue' ][ 0 ];
                    
                    // remove her of queue
                    data[ 'queue' ].splice( 0 , 1 );
                    data = null;
                    
                    // force animation and start
                    setting.force = true;
                    Animate.prototype.init.call( setting );
                    setting = null;
                }
            }
            
            function save ( element , array ) {
                var r = {} ,
                    manual = parseCss( element.style.cssText );
                
                array.forEach( function ( _ ) {
                    var inline = manual[ _ ];
                    
                    r[ _ ] = {
                        inline : inline ? inline.val : null ,
                        
                        computed : ( function () {
                            var tmp;
                            
                            !!inline && element.style.removeProperty( _ );
                            
                            tmp = getstyleproperty( element , _ );
                            
                            !!inline && element.style.setProperty( _ , inline.val , inline.imp );
                            
                            return tmp;
                        } )()
                    };
                } );
                
                return r;
            }
            
            function checkUselessProp ( element , saved , type ) {
                
                switch ( type ) {
                    
                    case 'show':
                        ( function () {
                            
                            var opacity = saved[ 'opacity' ];
                            if ( opacity.computed != 0 ||
                                ( opacity.inline != 0 && opacity.computed != 0 ) ) {
                                element.style.removeProperty( 'opacity' );
                            }
                            
                            var visibility = saved[ 'visibility' ];
                            if ( visibility.computed != 'hidden' ||
                                ( visibility.inline != 'hidden' && visibility.computed != 'hidden' ) ) {
                                element.style.removeProperty( 'visibility' );
                            }
                            
                            var display = saved[ 'display' ];
                            if ( display.computed != 'none' ||
                                ( display.inline != 'none' && display.computed != 'none' ) ) {
                                element.style.removeProperty( 'display' );
                            }
                            
                        } )();
                        break;
                    
                    case 'hide':
                        ( function () {
                            
                            element.style.removeProperty( 'opacity' );
                            element.style.removeProperty( 'visibility' );
                            
                            var display = saved[ 'display' ];
                            if ( display.computed == 'none' ) {
                                element.style.removeProperty( 'display' );
                            }
                            
                        } )();
                        break;
                    
                }
                
            }
            
            // animation events start / end
            ( function () {
                var i = 0 ,
                    eventEnd = [
                        'animationend' ,
                        'oanimationend' ,
                        'MSAnimationEnd' ,
                        'mozAnimationEnd' ,
                        'webkitAnimationEnd'
                    ] ,
                    eventStart = [
                        'animationstart' ,
                        'oanimationstart' ,
                        'MSAnimationStart' ,
                        'mozAnimationStart' ,
                        'webkitAnimationStart'
                    ];
                
                var capture = function ( cap ) {
                    if ( eventPassiveSupport ) {
                        return {
                            capture : !!cap ,
                            passive : true
                        };
                    }
                    
                    return !!cap;
                };
                
                for ( ; i < eventEnd.length ; i++ ) {
                    global.addEventListener( eventEnd[ i ] , _anime.animationEnd , capture( true ) );
                    global.addEventListener( eventStart[ i ] , _anime.animationStart , capture( true ) );
                }
            } )();
            
            function _anime ( setting ) {
                return new Animate( {
                    element : setting.element ,
                    
                    begin : setting.begin || null ,
                    done : setting.done || null ,
                    
                    how : setting.how || 'linear' ,
                    force : !!setting.force ,
                    merge : !!setting.merge ,
                    wait : !!setting.wait ,
                    
                    properties : setting.properties || null ,
                    duration : setting.duration || null ,
                    fingerprint : ( function () {
                        if ( !setting.properties || setting.wait ) {
                            return null;
                        }
                        
                        return fingerprint( setting.properties , setting.duration );
                    } )()
                } );
            }
            
            _anime.id = 0;
            _anime.rand = 0;
            _anime.busy = [];
            _anime.cache = {};
            _anime.element = {};
            _anime.prename = '_' + randomstring() + '-';
            
            _anime.prefix = ( function () {
                var test = document.createElement( 'DIV' ).style ,
                    
                    animation = isset( test[ 'animation' ] ) && '' ||
                        ( isset( test[ 'mozAnimation' ] ) || isset( test[ 'MozAnimation' ] ) ) && '-moz-' || // mozilla
                        ( isset( test[ 'oAnimation' ] ) || isset( test[ 'OAnimation' ] ) ) && '-o-' || // opera
                        isset( test[ 'webkitAnimation' ] ) && '-webkit-' || // webkit
                        '' , // native
                    
                    transform = ( isset( test[ 'mozTransform' ] ) || isset( test[ 'MozTransform' ] ) ) && '-moz-' || // mozilla
                        ( isset( test[ 'oTransform' ] ) || isset( test[ 'OTransform' ] ) ) && '-o-' || // opera
                        isset( test[ 'webkitTransform' ] ) && '-webkit-' || // webkit
                        isset( test[ 'msTransform' ] ) && '-ms-' || // microsoft
                        ''; // native
                
                return test = null, {
                    animation : animation ,
                    transform : transform
                };
            } )();
            
            _anime.animationStart = function ( e ) {
                var element = e.target , data;
                
                if ( !!element[ guid ] && !!_anime.element[ element[ guid ] ] ) {
                    e.stopPropagation();
                    
                    data = _anime.element[ element[ guid ] ][ e.animationName ];
                    
                    clearTimeout( data.timeout );
                    
                    // forcing end animation ( hidden or disabled element can freeze the animation event )
                    data.timeout = setTimeout( function () {
                        _anime.animationEnd( {
                            target : element ,
                            animationName : data.name
                        } , data );
                    } , data.ms );
                }
            };
            
            _anime.animationEnd = function ( e , b , p , nocall ) {
                var element = e.target , data;
                
                if ( !!b || ( !!element[ guid ] && !!_anime.element[ element[ guid ] ] ) ) {
                    !b && e.stopImmediatePropagation();
                    
                    if ( data = !!b ? b : _anime.element[ element[ guid ] ][ e.animationName ] ) {
                        clearTimeout( data.timeout );
                        
                        // set final css
                        !p && setCss( element , data.css );
                        
                        // remove keyframes class
                        element.classList.remove( e.animationName );
                        
                        // done callback
                        if ( !nocall && data.done ) {
                            callEnd( data.done , element , p );
                        }
                        
                        delete _anime.element[ element[ guid ] ][ e.animationName ];
                        
                        _anime.busy.splice( _anime.busy.indexOf( e.animationName ) , 1 );
                        
                        // clear cache if it's too big
                        if ( _anime.busy.length <= 0 && Object.keys( _anime.cache ).length > 500 ) {
                            _anime.cache = {};
                            _anime.style.reset();
                        }
                        
                        data = null;
                        !p && downQueue( element , p );
                    }
                }
            };
            
            _anime.style = ( function () {
                var style = document.createElement( 'STYLE' ) ,
                    head = document.getElementsByTagName( 'HEAD' )[ 0 ] ,
                    back;
                
                style.setAttribute( 'type' , 'text/css' );
                head.appendChild( style );
                
                back = ( function ( rule ) {
                    return {
                        add : function ( selector , property ) {
                            rule.insertRule( selector + '{' + property + '}' , rule.cssRules.length );
                        } ,
                        reset : function () {
                            while ( rule.cssRules.length ) {
                                rule.deleteRule( rule.cssRules.length - 1 );
                            }
                        }
                    };
                } )( style[ 'sheet' ] );
                
                return back;
            } )();
            
            function Animate ( setting ) {
                var merge = setting.merge ,
                    nguid , data , i , t;
                
                this.css = setting.properties;
                this.fingerprint = setting.fingerprint;
                this.duration = setting.duration;
                this.wait = setting.wait;
                this.done = setting.done;
                this.force = setting.force;
                this.node = setting.element;
                this.timing = setting.how;
                this.begin = setting.begin;
                
                // node guid
                if ( !( nguid = this.node[ guid ] ) ) {
                    this.node[ guid ] = nguid = ++_anime.id;
                }
                
                // animations indexer
                if ( !_anime.element[ nguid ] ) {
                    _anime.element[ nguid ] = {};
                }
                
                // indexer data
                this.data = _anime.element[ nguid ];
                
                // merge new animation with current animation
                if ( merge && this.data.current ) {
                    this.fingerprint = null;
                    
                    // before inst
                    data = this.data[ this.data.current.name ];
                    
                    // merge current css
                    for ( i in data.css ) {
                        t = getstyleproperty( this.node , i );
                        
                        if ( t == data.css[ i ] ) {
                            delete data.css[ i ];
                            continue;
                        }
                        
                        this.node.style.setProperty( i , t );
                    }
                    
                    // end current animation
                    _anime.animationEnd( {
                        target : this.node ,
                        animationName : data.name
                    } , data , true , true );
                    
                    // merge setting
                    this.css = overrideObject( data.css , this.css );
                    this.data.state = false;
                    
                    if ( this.done ) {
                        if ( isarray( data.done ) ) {
                            this.done = data.done.concat( this.done );
                        }
                        else {
                            this.done = [ data.done , this.done ];
                        }
                    }
                    else {
                        this.done = data.done;
                    }
                }
                
                this.init();
            }
            
            Animate.prototype = {
                
                gotoNext : function () {
                    downQueue( this.node );
                } ,
                
                pattern : function () {
                    return {
                        css : this.css ,
                        timeout : null ,
                        done : this.done ,
                        name : this.animationName ,
                        ms : parseInt( this.duration * 1.15 )
                    };
                } ,
                
                // wait...
                pause : function () {
                    var self = this;
                    
                    setTimeout( function () {
                        
                        // call and delete callback
                        !!self.data.done && callEnd( self.data.done , self.node );
                        
                        // continue animation if need, else state = false
                        self.gotoNext();
                        
                    } , this.duration );
                } ,
                
                // set animation data, before build keyframes
                init : function () {
                    // list of animation
                    if ( !this.data.queue ) {
                        this.data.queue = [];
                    }
                    
                    // if !force ( by library ) && animation state = true, register list
                    if ( !this.force && !!this.data.state ) {
                        return this.data.queue.push( this );
                    }
                    
                    // state = true, animation is begin !
                    this.data.state = true;
                    
                    // just for wait, no animation
                    if ( this.wait ) {
                        return this.pause();
                    }
                    
                    // before start callback, can stop animation
                    if ( isfunction( this.begin ) && this.begin.call( this.node ) === 'stop' ) {
                        return this.gotoNext();
                    }
                    
                    // create keyframe
                    this.genKeyframes();
                } ,
                
                start : function () {
                    _anime.busy.push( this.animationName );
                    this.data.current = this.data[ this.animationName ] = this.pattern();
                    this.defineTimeEnd().node.classList.add( this.animationName );
                } ,
                
                // generate keyframe
                genKeyframes : function () {
                    
                    // check cache, begin animation
                    if ( this.fingerprint && ( this.animationName = _anime.cache[ this.fingerprint ] ) ) {
                        return this.start();
                    }
                    
                    // animation name
                    this.animationName = _anime.prename + 'animation_' + _anime.rand++;
                    
                    // set css property
                    var to = '';
                    
                    forin( this.css , function ( key , value ) {
                        to += key + ':' + value + ';';
                    } );
                    
                    // generate new keyframes
                    _anime.style.add(
                        '@' + _anime.prefix.animation + 'keyframes ' + this.animationName ,
                        'to{' + to + '}'
                    );
                    
                    // generate new  css class
                    _anime.style.add(
                        '.' + this.animationName ,
                        _anime.prefix.animation + 'animation:' + this.animationName + ' ' +
                        this.duration + 'ms ' + this.timing + ' 0s 1 normal forwards running !important'
                    );
                    
                    // begin animation
                    this.start();
                    
                    // save cache
                    if ( this.fingerprint ) {
                        _anime.cache[ this.fingerprint ] = this.animationName;
                    }
                } ,
                
                defineTimeEnd : function () {
                    var self = this ,
                        data = this.data[ this.animationName ];
                    
                    // forcing end animation ( hidden or disabled element can freeze the animation event )
                    data.timeout = setTimeout( function () {
                        _anime.animationEnd( {
                            target : self.node ,
                            animationName : data.name
                        } , data );
                    } , data.ms );
                    
                    return this;
                }
                
            };
            
            return {
                
                kill : function ( element ) {
                    var nguid , data , current;
                    
                    if ( nguid = element[ guid ] ) {
                        
                        if ( data = _anime.element[ nguid ] ) {
                            
                            if ( current = data.current ) {
                                clearTimeout( current.timeout );
                                element.classList.remove( current.name );
                                _anime.busy.splice( _anime.busy.indexOf( current.name ) , 1 );
                            }
                            
                            delete _anime.element[ nguid ];
                        }
                        
                        stylesSnapshot.erase( element );
                        
                        delete element[ guid ];
                    }
                } ,
                
                animate : function ( element , css , ms , opt , call , merge ) {
                    // get callback
                    var back = isfunction( call ) && call ||
                        isfunction( opt ) && opt ||
                        isfunction( ms ) && ms ||
                        false ,
                        
                        tmp;
                    
                    merge = isboolean( ms ) ? ms :
                        isboolean( opt ) ? opt :
                            isboolean( call ) ? call :
                                !!merge;
                    
                    // get css property and timing if string
                    if ( isstring( css ) && isstring( ms ) ) {
                        tmp = css, css = {}, css[ tmp ] = ms;
                        ms = isinteger( opt ) && +opt || patternTime.origin;
                    }
                    else {
                        ms = isinteger( ms ) ? +ms : patternTime.origin;
                    }
                    
                    _anime( {
                        element : element ,
                        properties : css ,
                        duration : ms ,
                        done : back ,
                        merge : merge
                    } );
                } ,
                
                wait : function ( element , delay , call ) {
                    var data = dataAnim( delay , call );
                    
                    _anime( {
                        element : element ,
                        duration : data[ 1 ] ,
                        done : data[ 0 ] ,
                        wait : true
                    } );
                } ,
                
                fadeIn : function ( element , dur , call ) {
                    // get timing and callback
                    var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                        data = dataAnim( dur , call ) ,
                        back = data[ 0 ];
                    
                    _anime( {
                        element : element ,
                        duration : data[ 1 ] ,
                        properties : {
                            'opacity' : getRightOpacity( saved.opacity )
                        } ,
                        
                        begin : function () {
                            if ( !is_hidden( this ) ) {
                                if ( !!back ) {
                                    back.call( this );
                                }
                                
                                return 'stop';
                            }
                            
                            stylesSnapshot.make( this , [
                                "opacity" ,
                                "visibility"
                            ] );
                            
                            // before animation start , set first property of fade
                            setCss( this , {
                                "opacity" : "0" ,
                                "visibility" : "visible" ,
                                "display" : getVisibleDisplayValue( this )
                            } );
                        } ,
                        
                        done : function () {
                            checkUselessProp( element , saved , 'show' );
                            
                            stylesSnapshot.recover( this );
                            
                            if ( !!back ) {
                                back.call( this );
                            }
                        }
                    } );
                } ,
                
                fadeOut : function ( element , dur , call ) {
                    // get timing and callback
                    var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                        data = dataAnim( dur , call ) ,
                        back = data[ 0 ];
                    
                    _anime( {
                        element : element ,
                        duration : data[ 1 ] ,
                        properties : {
                            'opacity' : '0'
                        } ,
                        
                        begin : function () {
                            if ( is_hidden( this ) ) {
                                if ( !!back ) {
                                    back.call( this );
                                }
                                
                                return 'stop';
                            }
                            
                            sdp.set( this );
                            
                            stylesSnapshot.make( this , [
                                "opacity" ,
                                "pointer-events"
                            ] );
                            
                            setCss( this , {
                                "pointer-events" : "none"
                            } );
                        } ,
                        
                        done : function () {
                            checkUselessProp( element , saved , 'hide' );
                            
                            this.style.display = 'none';
                            
                            stylesSnapshot.recover( this );
                            
                            if ( !!back ) {
                                back.call( this );
                            }
                        }
                    } );
                } ,
                
                toggleFade : function ( element , dur , call ) {
                    if ( is_hidden( element ) ) {
                        this.fadeIn( element , dur , call );
                    }
                    else {
                        this.fadeOut( element , dur , call );
                    }
                } ,
                
                slideDown : function ( element , dur , call ) {
                    var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                        height = calcProp( element , 'height' ) ,
                        data = dataAnim( dur , call ) ,
                        back = data[ 0 ];
                    
                    _anime( {
                        element : element ,
                        duration : data[ 1 ] ,
                        properties : {
                            "height" : height ,
                            "margin-top" : getstyleproperty( element , "margin-top" ) ,
                            "padding-top" : getstyleproperty( element , "padding-top" ) ,
                            "margin-bottom" : getstyleproperty( element , "margin-bottom" ) ,
                            "padding-bottom" : getstyleproperty( element , "padding-bottom" )
                        } ,
                        
                        begin : function () {
                            if ( !is_hidden( this ) ) {
                                if ( !!back ) {
                                    back.call( this );
                                }
                                
                                return 'stop';
                            }
                            
                            stylesSnapshot.make( this , [
                                "height" ,
                                "overflow" ,
                                "margin-top" ,
                                "padding-top" ,
                                "margin-bottom" ,
                                "padding-bottom"
                            ] );
                            
                            setCss( this , {
                                "height" : "0" ,
                                "margin-top" : "0" ,
                                "padding-top" : "0" ,
                                "overflow" : "hidden" ,
                                "margin-bottom" : "0" ,
                                "padding-bottom" : "0" ,
                                "display" : getVisibleDisplayValue( this )
                            } );
                        } ,
                        
                        done : function ( p ) {
                            if ( !p ) {
                                stylesSnapshot.recover( this );
                            }
                            
                            checkUselessProp( element , saved , 'show' );
                            
                            if ( !!back ) {
                                back.call( this );
                            }
                        }
                    } );
                } ,
                
                slideUp : function ( element , dur , call ) {
                    var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                        data = dataAnim( dur , call ) ,
                        back = data[ 0 ];
                    
                    _anime( {
                        element : element ,
                        duration : data[ 1 ] ,
                        properties : {
                            "height" : "0" ,
                            "margin-top" : "0" ,
                            "padding-top" : "0" ,
                            "margin-bottom" : "0" ,
                            "padding-bottom" : "0"
                        } ,
                        
                        begin : function () {
                            if ( is_hidden( this ) ) {
                                if ( !!back ) {
                                    back.call( this );
                                }
                                
                                return 'stop';
                            }
                            
                            sdp.set( this );
                            
                            stylesSnapshot.make( this , [
                                "height" ,
                                "overflow" ,
                                "margin-top" ,
                                "padding-top" ,
                                "margin-bottom" ,
                                "padding-bottom" ,
                                "pointer-events"
                            ] );
                            
                            setCss( this , {
                                "overflow" : "hidden" ,
                                "pointer-events" : "none" ,
                                "height" : ( this.clientHeight || this.offsetHeight ) + 'px'
                            } );
                        } ,
                        
                        done : function ( p ) {
                            checkUselessProp( element , saved , 'hide' );
                            
                            if ( !p ) {
                                this.style.display = 'none';
                                stylesSnapshot.recover( this );
                            }
                            
                            if ( !!back ) {
                                back.call( this );
                            }
                        }
                    } );
                } ,
                
                slideRight : function ( element , dur , call ) {
                    var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                        width = calcProp( element , 'width' ) ,
                        data = dataAnim( dur , call ) ,
                        back = data[ 0 ];
                    
                    _anime( {
                        element : element ,
                        duration : data[ 1 ] ,
                        properties : {
                            "width" : width ,
                            "margin-left" : getstyleproperty( element , "margin-left" ) ,
                            "padding-left" : getstyleproperty( element , "padding-left" ) ,
                            "margin-right" : getstyleproperty( element , "margin-right" ) ,
                            "padding-right" : getstyleproperty( element , "padding-right" )
                        } ,
                        
                        begin : function () {
                            if ( !is_hidden( this ) ) {
                                if ( !!back ) {
                                    back.call( this );
                                }
                                
                                return 'stop';
                            }
                            
                            stylesSnapshot.make( this , [
                                "width" ,
                                "overflow" ,
                                "margin-left" ,
                                "padding-left" ,
                                "margin-right" ,
                                "padding-right"
                            ] );
                            
                            setCss( this , {
                                "width" : "0" ,
                                "margin-left" : "0" ,
                                "padding-left" : "0" ,
                                "margin-right" : "0" ,
                                "overflow" : "hidden" ,
                                "padding-right" : "0" ,
                                "display" : getVisibleDisplayValue( this )
                            } );
                        } ,
                        
                        done : function () {
                            checkUselessProp( element , saved , 'show' );
                            
                            stylesSnapshot.recover( this );
                            
                            if ( !!back ) {
                                back.call( this );
                            }
                        }
                    } );
                } ,
                
                slideLeft : function ( element , dur , call ) {
                    var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                        data = dataAnim( dur , call ) ,
                        back = data[ 0 ];
                    
                    _anime( {
                        element : element ,
                        duration : data[ 1 ] ,
                        properties : {
                            "width" : "0" ,
                            "margin-left" : "0" ,
                            "padding-left" : "0" ,
                            "margin-right" : "0" ,
                            "padding-right" : "0"
                        } ,
                        
                        begin : function () {
                            if ( is_hidden( this ) ) {
                                if ( !!back ) {
                                    back.call( this );
                                }
                                
                                return 'stop';
                            }
                            
                            sdp.set( this );
                            
                            stylesSnapshot.make( this , [
                                "width" ,
                                "overflow" ,
                                "margin-left" ,
                                "padding-left" ,
                                "margin-right" ,
                                "padding-right" ,
                                "pointer-events"
                            ] );
                            
                            setCss( this , {
                                "overflow" : "hidden" ,
                                "pointer-events" : "none" ,
                                "width" : ( this.clientWidth || this.offsetWidth ) + 'px'
                            } );
                        } ,
                        
                        done : function () {
                            checkUselessProp( element , saved , 'hide' );
                            
                            this.style.display = 'none';
                            stylesSnapshot.recover( this );
                            
                            if ( !!back ) {
                                back.call( this );
                            }
                        }
                    } );
                } ,
                
                toggleSlide : function ( element , dur , call ) {
                    if ( is_hidden( element ) ) {
                        this.slideDown( element , dur , call );
                    }
                    else {
                        this.slideUp( element , dur , call );
                    }
                } ,
                
                turnIn : function ( element , dur , call ) {
                    // get timing and callback
                    var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                        data = dataAnim( dur , call ) ,
                        back = data[ 0 ];
                    
                    var css = {
                        'opacity' : getRightOpacity( saved.opacity )
                    };
                    
                    css[ _anime.prefix.transform + 'transform' ] = 'rotateY(0deg) rotateX(0deg)';
                    
                    _anime( {
                        element : element ,
                        duration : data[ 1 ] ,
                        how : 'ease' ,
                        properties : css ,
                        
                        begin : function () {
                            if ( !is_hidden( this ) ) {
                                if ( !!back ) {
                                    back.call( this );
                                }
                                
                                return 'stop';
                            }
                            
                            stylesSnapshot.make( this , [
                                "opacity" ,
                                "visibility"
                            ] );
                            
                            // before start set first property of turn
                            var css = {
                                'opacity' : '0' ,
                                'visibility' : 'visible' ,
                                'display' : getVisibleDisplayValue( this )
                            };
                            
                            css[ _anime.prefix.transform + 'transform' ] = 'rotateY(90deg) rotateX(90deg)';
                            
                            setCss( this , css );
                        } ,
                        
                        done : function () {
                            checkUselessProp( element , saved , 'show' );
                            
                            var css = {};
                            
                            css[ _anime.prefix.transform + 'transform' ] = '';
                            
                            setCss( this , css );
                            
                            stylesSnapshot.recover( this );
                            
                            if ( !!back ) {
                                back.call( this );
                            }
                        }
                    } );
                } ,
                
                turnOut : function ( element , dur , call ) {
                    // get timing and callback
                    var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                        data = dataAnim( dur , call ) ,
                        back = data[ 0 ];
                    
                    var css = {
                        'opacity' : '0'
                    };
                    
                    css[ _anime.prefix.transform + 'transform' ] = 'rotateY(90deg) rotateX(90deg)';
                    
                    _anime( {
                        element : element ,
                        duration : data[ 1 ] ,
                        how : 'ease' ,
                        properties : css ,
                        
                        begin : function () {
                            if ( is_hidden( this ) ) {
                                if ( !!back ) {
                                    back.call( this );
                                }
                                
                                return 'stop';
                            }
                            
                            sdp.set( this );
                            
                            stylesSnapshot.make( this , [
                                "opacity" ,
                                "pointer-events"
                            ] );
                            
                            setCss( this , {
                                "pointer-events" : "none"
                            } );
                        } ,
                        
                        done : function () {
                            checkUselessProp( element , saved , 'hide' );
                            
                            var css = { 'display' : 'none' };
                            
                            css[ _anime.prefix.transform + 'transform' ] = '';
                            
                            setCss( this , css );
                            
                            stylesSnapshot.recover( this );
                            
                            if ( !!back ) {
                                back.call( this );
                            }
                        }
                    } );
                } ,
                
                toggleTurn : function ( element , dur , call ) {
                    if ( is_hidden( element ) ) {
                        this.turnIn( element , dur , call );
                    }
                    else {
                        this.turnOut( element , dur , call );
                    }
                } ,
                
                plopIn : function ( element , dur , call ) {
                    // get timing and callback
                    var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                        data = dataAnim( dur , call ) ,
                        back = data[ 0 ];
                    
                    var css = {
                        'opacity' : getRightOpacity( saved.opacity )
                    };
                    
                    css[ _anime.prefix.transform + 'transform' ] = 'scale(1)';
                    
                    _anime( {
                        element : element ,
                        duration : data[ 1 ] ,
                        how : 'ease' ,
                        properties : css ,
                        
                        begin : function () {
                            if ( !is_hidden( this ) ) {
                                if ( !!back ) {
                                    back.call( this );
                                }
                                
                                return 'stop';
                            }
                            
                            stylesSnapshot.make( this , [
                                "opacity" ,
                                "visibility"
                            ] );
                            
                            // before start set first property of turn
                            var css = {
                                'opacity' : '0' ,
                                'visibility' : 'visible' ,
                                'display' : getVisibleDisplayValue( this )
                            };
                            
                            css[ _anime.prefix.transform + 'transform' ] = 'scale(.8)';
                            
                            setCss( this , css );
                        } ,
                        
                        done : function () {
                            checkUselessProp( element , saved , 'show' );
                            
                            var css = {};
                            
                            css[ _anime.prefix.transform + 'transform' ] = '';
                            
                            setCss( this , css );
                            
                            stylesSnapshot.recover( this );
                            
                            if ( !!back ) {
                                back.call( this );
                            }
                        }
                    } );
                } ,
                
                plopOut : function ( element , dur , call ) {
                    // get timing and callback
                    var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                        data = dataAnim( dur , call ) ,
                        back = data[ 0 ];
                    
                    var css = {
                        'opacity' : '0'
                    };
                    
                    css[ _anime.prefix.transform + 'transform' ] = 'scale(.8)';
                    
                    _anime( {
                        element : element ,
                        duration : data[ 1 ] ,
                        how : 'ease' ,
                        properties : css ,
                        
                        begin : function () {
                            if ( is_hidden( this ) ) {
                                if ( !!back ) {
                                    back.call( this );
                                }
                                
                                return 'stop';
                            }
                            
                            sdp.set( this );
                            
                            stylesSnapshot.make( this , [
                                "opacity" ,
                                "pointer-events"
                            ] );
                            
                            setCss( this , {
                                "pointer-events" : "none"
                            } );
                        } ,
                        
                        done : function () {
                            checkUselessProp( element , saved , 'hide' );
                            
                            var css = { 'display' : 'none' };
                            
                            css[ _anime.prefix.transform + 'transform' ] = '';
                            
                            setCss( this , css );
                            
                            stylesSnapshot.recover( this );
                            
                            if ( !!back ) {
                                back.call( this );
                            }
                        }
                    } );
                } ,
                
                togglePlop : function ( element , dur , call ) {
                    if ( is_hidden( element ) ) {
                        this.plopIn( element , dur , call );
                    }
                    else {
                        this.plopOut( element , dur , call );
                    }
                } ,
                
                show : function ( element ) {
                    element.style.display = getVisibleDisplayValue( element );
                } ,
                
                hide : function ( element ) {
                    sdp.set( element );
                    element.style.display = 'none';
                } ,
                
                toggle : function ( element , dur , call ) {
                    if ( is_hidden( element ) ) {
                        this.show( element , dur , call );
                    }
                    else {
                        this.hide( element , dur , call );
                    }
                }
                
            };
        }
        
        /**
         * @public
         * @readonly
         * @function animationBuild
         * */
        _defineProperty( global , 'animationBuild' , function ( d , w ) {
            return build( getdocument( d ) || global.document , getwindow( w ) || global );
        } );
        
    } )();
    
    /**
     * vanillajax
     * @license MIT
     * @see {@link https://framagit.org/tla/vanillajax}
     * */
    ( function () {
        
        function encodeValue ( value ) {
            if ( exist( value ) ) {
                return '=' + encodeURIComponent( value );
            }
            
            return '';
        }
        
        function formatFormData ( data ) {
            var r = [];
            
            forin( data , function ( key , value ) {
                r.push( encodeURIComponent( key ) + encodeValue( value ) );
            } );
            
            return r.join( '&' );
        }
        
        function formatQueryString ( url , queries ) {
            var r = [];
            
            forin( queries , function ( key , value ) {
                r.push( encodeURIComponent( key ) + encodeValue( value ) );
            } );
            
            if ( r.length ) {
                url += ( url.indexOf( '?' ) !== -1 ? '&' : '?' ) + r.join( '&' );
            }
            
            return url;
        }
        
        function parseArguments ( url , callback , proxy ) {
            var tmp;
            
            if ( isobject( url ) ) {
                return url;
            }
            
            tmp = { url : url };
            
            if ( isfunction( callback ) ) {
                tmp.anyway = function ( x ) {
                    proxy( callback , x );
                };
            }
            
            return tmp;
        }
        
        function toDocumentFragment ( HTMLDocument ) {
            var frag , children;
            
            if ( !exist( HTMLDocument ) ||
                !( children = HTMLDocument.body.children ).length ) {
                return null;
            }
            
            frag = document.createDocumentFragment();
            
            while ( children.length ) {
                frag.appendChild( children[ 0 ] );
            }
            
            return frag;
        }
        
        /**
         * @readonly
         * @function ajax
         * @param {Object} setting
         *
         * @param {*} [setting.brut]
         * @param {Object} [setting.get]
         * @param {Object} [setting.post]
         *
         * @param {string} setting.url
         * @param {boolean} [setting.async=true]
         * @param {boolean} [setting.cache=true]
         * @param {boolean} [setting.crossDomain=false]
         * @param {boolean} [setting.withCredentials=false]
         *
         * @param {string} [setting.type=GET]
         * @param {Object} [setting.headers]
         * @param {number} [setting.timeoutDelay=90 seconds]
         *
         * @param {string} [setting.mimeType]
         * @param {string} [setting.responseType=text]
         *
         * @param {function} [setting.done]
         * @param {function} [setting.then]
         *
         * @param {function} [setting.error]
         * @param {function} [setting.fail]
         *
         * @param {function} [setting.abort]
         * @param {function} [setting.aborted]
         *
         * @param {function} [setting.timeout]
         * @param {function} [setting.timeup]
         *
         * @param {function} [setting.anyway]
         * @param {function} [setting.finally]
         *
         * @return {!Object}
         * */
        _defineProperty( global , 'ajax' , function ( setting ) {
            var x = new XMLHttpRequest() ,
                index = ( ++ajax.idx ) ,
                defaultHeaders = {} ,
                formData = null ,
                timeout;
            
            // default
            // ---------------------------
            setting = overrideObject( {
                url : null ,
                
                get : null ,
                post : null ,
                brut : null ,
                
                async : true ,
                cache : true ,
                crossDomain : false ,
                withCredentials : false ,
                
                type : 'GET' ,
                headers : {} ,
                timeoutDelay : 90 ,
                
                mimeType : null ,
                responseType : 'text'
            } , setting );
            
            if ( !isstring( setting.url ) ) {
                throw 'ajax error : setting.url argument cannot be empty.';
            }
            
            // callbacks
            // ---------------------------
            var calltypes = [];
            
            var calltypesAlias = {
                'done' : [ 'then' ] ,
                'error' : [ 'fail' ] ,
                'abort' : [ 'aborted' ] ,
                'timeout' : [ 'timeup' ] ,
                'anyway' : [ 'finally' ]
            };
            
            forin( calltypesAlias , function ( type , alias ) {
                calltypes.push( type );
                Array.prototype.push.apply( calltypes , alias );
            } );
            
            function getCallbackType ( key ) {
                if ( isfillstring( key ) ) {
                    
                    if ( calltypesAlias[ key ] ) {
                        return key;
                    }
                    
                    for ( var type in calltypesAlias ) {
                        if ( inarray( key , calltypesAlias[ type ] ) ) {
                            return type;
                        }
                    }
                    
                }
                
                return null;
            }
            
            var callbacks = {
                
                stack : function ( type , callback ) {
                    type = getCallbackType( type );
                    
                    if ( type && isfunction( callback ) ) {
                        
                        if ( !callbacks[ type ] ) {
                            callbacks[ type ] = [];
                        }
                        
                        if ( setting.override && setting.override[ type ] ) {
                            
                            return callbacks[ type ].push( ( function ( bypass ) {
                                
                                return function ( x ) {
                                    bypass.call( x , x , callback );
                                };
                                
                            } )( setting.override[ type ] ) );
                            
                        }
                        
                        callbacks[ type ].push( callback );
                        
                    }
                } ,
                
                unstack : function ( type , xhr ) {
                    type = getCallbackType( type );
                    
                    if ( callbacks[ type ] && callbacks[ type ].length ) {
                        
                        callbacks[ type ].forEach( function ( callback ) {
                            callback.call( xhr , xhr );
                        } );
                        
                        callbacks[ type ] = [];
                        
                        return true;
                        
                    }
                    
                    return false;
                }
                
            };
            
            calltypes.forEach( function ( type ) {
                callbacks.stack( type , setting[ type ] );
            } );
            
            // settings
            // ---------------------------
            
            // http request
            setting.type = setting.type.toUpperCase();
            
            // with credentials
            if ( isboolean( setting.withCredentials ) ) {
                x.withCredentials = setting.withCredentials;
            }
            
            // format get arguments
            if ( isobject( setting.get ) ) {
                setting.url = formatQueryString( setting.url , setting.get );
            }
            
            // format post arguments
            if ( isobject( setting.post ) ) {
                formData = formatFormData( setting.post );
                
                if ( /^(GET|HEAD|TRACE|OPTIONS|CONNECT)$/.test( setting.type ) ) {
                    setting.type = 'POST';
                }
            }
            
            // set brut data
            if ( exist( setting.brut ) ) {
                formData = setting.brut;
                
                if ( setting.type !== 'BRUT' ) {
                    setting.type = 'BRUT';
                }
            }
            
            // set responseType
            if ( isstring( setting.responseType ) ) {
                try {
                    x.responseType = setting.responseType;
                } catch ( _ ) {
                    x.responseType = 'text';
                }
            }
            
            // mimetype
            if ( isstring( setting.mimeType ) ) {
                x.overrideMimeType( setting.mimeType );
            }
            
            // request destructor
            // ---------------------------
            function destroy () {
                if ( index && isfunction( ajax.request[ index ] ) ) {
                    ajax.request[ index ]();
                }
            }
            
            x.aborted = false;
            
            ajax.request[ index ] = function () {
                if ( !x || !index ) {
                    return;
                }
                
                delete ajax.request[ index ];
                clearTimeout( timeout );
                
                if ( !x.aborted && x.readyState < 4 ) {
                    x.aborted = true;
                    x.abort();
                }
                
                ajax.active--;
                index = null;
            };
            
            // abort
            // ---------------------------
            x.addEventListener( 'abort' , function () {
                destroy();
                
                callbacks.unstack( 'abort' , x );
                callbacks.unstack( 'anyway' , x );
            } , { once : true } );
            
            // error
            // ---------------------------
            x.addEventListener( 'error' , function () {
                destroy();
                
                callbacks.unstack( 'error' , x );
                callbacks.unstack( 'anyway' , x );
            } , { once : true } );
            
            // timeout
            // ---------------------------
            x.addEventListener( 'timeout' , function () {
                destroy();
                
                if ( !callbacks.unstack( 'timeout' , x ) ) {
                    callbacks.unstack( 'error' , x );
                }
                
                callbacks.unstack( 'anyway' , x );
            } , { once : true } );
            
            if ( isinteger( setting.timeoutDelay ) && setting.timeoutDelay >= 0 ) {
                x.timeout = setting.timeoutDelay * 1000;
            }
            
            // response
            // ---------------------------
            x.addEventListener( 'load' , function () {
                destroy();
                
                // server / request error
                if ( /^[045]/g.test( x.status + '' ) ) {
                    callbacks.unstack( 'error' , x );
                }
                // all good
                else {
                    callbacks.unstack( 'done' , x );
                }
                
                callbacks.unstack( 'anyway' , x );
            } , { once : true } );
            
            // create request
            // ---------------------------
            switch ( setting.type ) {
                
                case 'BRUT':
                    ( function () {
                        defaultHeaders[ 'Content-Type' ] = 'multipart/form-data';
                        x.open( 'POST' , setting.url , setting.async );
                    } )();
                    break;
                
                case 'PUT':
                case 'POST':
                case 'PATCH':
                case 'DELETE':
                case 'UPDATE':
                    ( function () {
                        defaultHeaders[ 'Content-Type' ] = 'application/x-www-form-urlencoded';
                        x.open( setting.type , setting.url , setting.async );
                    } )();
                    break;
                
                case 'GET':
                case 'HEAD':
                case 'TRACE':
                case 'OPTIONS':
                case 'CONNECT':
                    ( function () {
                        // disable cache
                        if ( !setting.cache ) {
                            defaultHeaders[ 'Cache-Control' ] = 'no-cache, no-store, must-revalidate';
                            defaultHeaders[ 'Pragma' ] = 'no-cache';
                            defaultHeaders[ 'Expires' ] = '0';
                            
                            setting.url = formatQueryString( setting.url , {
                                '__' : Date.now()
                            } );
                        }
                        
                        x.open( setting.type , setting.url , setting.async );
                    } )();
                    break;
                
            }
            
            // cross domain request
            // ---------------------------
            if ( setting.crossDomain ) {
                defaultHeaders[ 'Accept' ] = '*/*';
            }
            else {
                defaultHeaders[ 'X-Requested-With' ] = 'XMLHttpRequest';
            }
            
            // headers
            // ---------------------------
            forin( overrideObject( defaultHeaders , setting.headers ) , function ( key , value ) {
                x.setRequestHeader( key , value );
            } );
            
            // send
            // ---------------------------
            ajax.active++;
            x.send( formData );
            
            // actions
            // ---------------------------
            var result = {
                xhr : x ,
                setting : setting
            };
            
            calltypes.forEach( function ( type ) {
                result[ type ] = function ( callback ) {
                    callbacks.stack( type , callback );
                    return result;
                };
            } );
            
            result.abort = function ( callback ) {
                if ( isfunction( callback ) ) {
                    callbacks.stack( 'abort' , callback );
                    return result;
                }
                
                destroy();
            };
            
            return result;
        } );
        
        /**
         * @private
         * @property {Object} ajax.request
         * */
        _defineProperty( ajax , 'request' , {} );
        
        /**
         * @property {number} ajax.active
         * */
        ajax.active = 0;
        
        /**
         * @private
         * @property {number} ajax.idx
         * */
        ajax.idx = 0;
        
        /**
         * @readonly
         * @function getText
         * @param {string|Object} url
         * @param {function} [callback]
         * @return {!Object}
         * */
        _defineProperty( global , 'getText' , function ( url , callback ) {
            return ajax( overrideObject(
                parseArguments(
                    // user settings
                    url , callback ,
                    
                    // anyway
                    function ( callback , x ) {
                        callback.call( x , x.response || '' , x );
                    }
                ) ,
                
                // default settings
                {
                    responseType : 'text' ,
                    override : {
                        done : function ( x , origin ) {
                            origin.call( x , x.response || '' , x );
                        } ,
                        error : function ( x , origin ) {
                            origin.call( x , x.response || '' , x );
                        }
                    }
                }
            ) );
        } );
        
        /**
         * @readonly
         * @function getJson
         * @param {string|Object} url
         * @param {function} [callback]
         * @return {!Object}
         * */
        _defineProperty( global , 'getJson' , function ( url , callback ) {
            return ajax( overrideObject(
                parseArguments(
                    // user settings
                    url , callback ,
                    
                    // anyway
                    function ( callback , x ) {
                        callback.call( x , x.response || null , x );
                    }
                ) ,
                
                // default settings
                {
                    responseType : 'json' ,
                    override : {
                        done : function ( x , origin ) {
                            origin.call( x , x.response || null , x );
                        } ,
                        error : function ( x , origin ) {
                            origin.call( x , x.response || null , x );
                        }
                    }
                }
            ) );
        } );
        
        /**
         * @readonly
         * @function getXML
         * @param {string|Object} url
         * @param {function} [callback]
         * @return {!Object}
         * */
        _defineProperty( global , 'getXML' , function ( url , callback ) {
            return ajax( overrideObject(
                parseArguments(
                    // user settings
                    url , callback ,
                    
                    // anyway
                    function ( callback , x ) {
                        callback.call( x , x.response || null , x );
                    }
                ) ,
                
                // default settings
                {
                    mimeType : 'text/xml' ,
                    responseType : 'document' ,
                    override : {
                        done : function ( x , origin ) {
                            origin.call( x , x.response || null , x );
                        } ,
                        error : function ( x , origin ) {
                            origin.call( x , x.response || null , x );
                        }
                    }
                }
            ) );
        } );
        
        /**
         * @readonly
         * @function getHTML
         * @param {string|Object} url
         * @param {function} [callback]
         * @return {!Object}
         * */
        _defineProperty( global , 'getHTML' , function ( url , callback ) {
            return ajax( overrideObject(
                parseArguments(
                    // user settings
                    url , callback ,
                    
                    // anyway
                    function ( callback , x ) {
                        callback.call( x , toDocumentFragment( x.response ) , x );
                    }
                ) ,
                
                // default settings
                {
                    mimeType : 'text/html' ,
                    responseType : 'document' ,
                    override : {
                        done : function ( x , origin ) {
                            origin.call( x , toDocumentFragment( x.response ) , x );
                        } ,
                        error : function ( x , origin ) {
                            origin.call( x , toDocumentFragment( x.response ) , x );
                        }
                    }
                }
            ) );
        } );
        
        /**
         * @readonly
         * @function getFile
         * @param {string|Object} url
         * @param {function} [callback]
         * @return {!Object}
         * */
        _defineProperty( global , 'getFile' , function ( url , callback ) {
            return ajax( overrideObject(
                parseArguments(
                    // user settings
                    url , callback ,
                    
                    // anyway
                    function ( callback , x ) {
                        callback.call( x , x.response || null , x );
                    }
                ) ,
                
                // default settings
                {
                    responseType : 'blob' ,
                    override : {
                        done : function ( x , origin ) {
                            origin.call( x , x.response || null , x );
                        } ,
                        error : function ( x , origin ) {
                            origin.call( x , x.response || null , x );
                        }
                    }
                }
            ) );
        } );
        
    } )();
    
    // ----- Kitten
    // ------------------------------------------------------------
    // ------------------------------------------------------------
    
    /**
     * @class Kitten - OMFGWTFBBQ
     * @augments Array
     * @typedef {!Kitten#} KittenInstance
     * */
    function Kitten () {
    }
    
    /**
     * @property {Kitten.prototype} fn
     * @memberof Kitten
     * */
    _defineProperty( Kitten , 'fn' , Kitten.prototype = new Array() );
    
    /**
     * @property {Kitten} constructor
     * @memberof Kitten
     * */
    _defineProperty( Kitten , 'constructor' , Kitten );
    
    /**
     * @property {Kitten} constructor
     * @memberof Kitten.prototype
     * */
    _defineProperty( Kitten.prototype , 'constructor' , Kitten );
    
    /**
     * @readonly
     * @property {eventHandler#} eventHandler
     * @memberof Kitten
     * */
    _defineProperty( Kitten , 'eventHandler' , new eventHandler() );
    
    /**
     * @readonly
     * @property {Object} animationHandler
     * @memberof Kitten
     * */
    _defineProperty( Kitten , 'animationHandler' , animationBuild() );
    
    /**
     * @readonly
     * @override
     * @function {target.closest} closest
     * @memberof Kitten.eventHandler#
     * */
    _defineProperty( Kitten.eventHandler , 'closest' , target.closest );
    
    /**
     * @function iskitten
     * @param {*} _
     * @return {!boolean}
     * */
    _defineProperty( global , 'iskitten' , function ( _ ) {
        return _ instanceof Kitten;
    } );
    
    // ----- create Kitten instance
    // ------------------------------------------------------------
    // ------------------------------------------------------------
    
    /**
     * @readonly
     * @function factory
     * @param {Array} [array]
     * @memberof Kitten
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten , 'factory' , function ( array ) {
        var kitten = new Kitten();
        
        if ( isarray( array ) ) {
            concat.apply( kitten , array );
        }
        
        return kitten;
    } );
    
    /**
     * @readonly
     * @function picker
     * @param {*} [arg]
     * @memberof Kitten
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten , 'picker' , function ( arg ) {
        var tmp , factory = Kitten.factory;
        
        // nothing ? m'okay.
        if ( !exist( arg ) ) {
            return factory();
        }
        
        // Kitten instance
        if ( iskitten( arg ) ) {
            return factory( clonearray( arg ) );
        }
        
        if ( isstring( arg ) ) {
            
            // html string
            if ( tmp = htmlDOM( arg ) ) {
                return factory( toarray( tmp ) );
            }
            
            // css selector
            return factory( target( arg ) );
        }
        
        if (
            // node
            isnode( arg ) ||
            
            // document
            isdocument( arg ) ||
            
            // window
            iswindow( arg )
        ) {
            return factory( [ arg ] );
        }
        
        // document fragment
        if ( isfragment( arg ) ) {
            return factory( fragmenttoarray( arg ) );
        }
        
        // nodelist
        if ( isnodelist( arg ) ) {
            return factory( typedtoarray( arg ) );
        }
        
        // dom ready
        if ( isfunction( arg ) ) {
            domLoad( arg );
        }
        
        // array of... things
        if ( isarray( arg ) ) {
            return ( function () {
                var kit = new Kitten();
                
                foreach( arg , function ( item ) {
                    concat.apply( kit , Kitten.picker( item ) );
                } );
                
                return kit;
            } )();
        }
        
        // no compatible argument
        return factory();
    } );
    
    // ----- patterns
    // ------------------------------------------------------------
    // ------------------------------------------------------------
    
    var DomMutationAppend = {
        
        insertAdjacentSupport : isfunction( document.createElement( 'div' ).insertAdjacentElement ) ,
        
        append : function ( container , element ) {
            container.appendChild( element );
        } ,
        
        appendBefore : function ( container , element ) {
            var parent;
            
            if ( DomMutationAppend.insertAdjacentSupport ) {
                return container.insertAdjacentElement( 'beforebegin' , element );
            }
            
            if ( parent = container.parentNode ) {
                parent.insertBefore( element , container );
            }
        } ,
        
        appendAfter : function ( container , element ) {
            var parent , next;
            
            if ( DomMutationAppend.insertAdjacentSupport ) {
                return container.insertAdjacentElement( 'afterend' , element );
            }
            
            if ( parent = container.parentNode ) {
                
                if ( next = container.nextElementSibling ) {
                    parent.insertBefore( element , next );
                }
                else {
                    parent.appendChild( element );
                }
                
            }
        } ,
        
        prepend : function ( container , element ) {
            var firstchild;
            
            if ( DomMutationAppend.insertAdjacentSupport ) {
                return container.insertAdjacentElement( 'afterbegin' , element );
            }
            
            if ( firstchild = container.firstElementChild ) {
                container.insertBefore( element , firstchild );
            }
            else {
                container.appendChild( element );
            }
        } ,
        
        factory : function ( insertType ) {
            var fn = DomMutationAppend[ insertType ];
            
            return function ( arg , cloneme ) {
                var element , nodes = Kitten.picker( arg );
                
                if ( this.length && nodes.length ) {
                    
                    // duplicate and append argument
                    if ( isstring( arg ) || cloneme ) {
                        return this.loop( function ( element ) {
                            var clones = nodes.clone( true );
                            
                            for ( var i = 0 , l = clones.length ; i < l ; i++ ) {
                                fn( element , clones[ i ] );
                            }
                        } );
                    }
                    
                    // append argument
                    element = this[ 0 ];
                    
                    for ( var i = 0 , l = nodes.length ; i < l ; i++ ) {
                        fn( element , nodes[ i ] );
                    }
                    
                }
                
                return this;
            };
        }
        
    };
    
    var DomMutationMove = {
        
        factory : function ( insertType ) {
            var fn = DomMutationAppend[ insertType ];
            
            return function ( destination , cloneme ) {
                var self = this ,
                    target = Kitten.picker( destination );
                
                if ( target.length ) {
                    if ( !!cloneme ) {
                        var result = clonearray( this );
                        
                        target.loop( function ( element ) {
                            var clones = self.clone( true );
                            concat.apply( result , clones );
                            
                            for ( var i = 0 , l = clones.length ; i < l ; i++ ) {
                                fn( element , clones[ i ] );
                            }
                        } );
                        
                        return Kitten.factory( result );
                    }
                    else {
                        target = target[ 0 ];
                        
                        return this.loop( function ( element ) {
                            fn( target , element );
                        } );
                    }
                }
                
                return this;
            };
            
        }
        
    };
    
    var ClassList = {
        
        factory : function ( name , arraysupport ) {
            
            function call ( element ) {
                return element.classList[ name ];
            }
            
            return function ( value , force ) {
                
                if ( isstring( value ) ) {
                    value = splitTrim( value , ',' );
                }
                
                if ( isarray( value ) ) {
                    if ( arraysupport ) {
                        return this.loop( function ( element ) {
                            call( element ).apply( element.classList , value );
                        } );
                    }
                    else {
                        return this.loop( function ( element ) {
                            for ( var i = 0 , l = value.length ; i < l ; i++ ) {
                                element.classList[ name ]( value[ i ] , force );
                            }
                        } );
                    }
                }
                
                return this;
            };
            
        }
        
    };
    
    var KeyValue = {
        
        factory : function ( actions ) {
            
            return function ( name , value ) {
                var first;
                
                if ( isstring( name ) ) {
                    
                    if ( isset( value ) ) {
                        return this.loop( function ( element ) {
                            actions.set( element , name , value );
                        } );
                    }
                    else {
                        if ( first = this[ 0 ] ) {
                            return actions.get( first , name );
                        }
                        
                        return null;
                    }
                    
                }
                
                if ( isobject( name ) ) {
                    return this.loop( function ( element ) {
                        forin( name , function ( i , name ) {
                            actions.set( element , i , name );
                        } );
                    } );
                }
                
                if ( isarray( name ) ) {
                    var result = [];
                    
                    if ( first = this[ 0 ] ) {
                        for ( var i = 0 , l = name.length ; i < l ; i++ ) {
                            result.push( actions.get( first , name[ i ] ) );
                        }
                        
                        return result;
                    }
                    
                    return null;
                }
                
                return this;
            };
            
        }
        
    };
    
    var Content = {
        
        factory : function ( property ) {
            return function ( content ) {
                var first;
                
                if ( isstring( content ) ) {
                    return this.loop( function ( element ) {
                        element[ property ] = content;
                    } );
                }
                
                if ( first = this[ 0 ] ) {
                    return first[ property ];
                }
            };
        }
        
    };
    
    var ScrollAPI = ( function () {
        
        function scrollMax ( element , dir ) {
            var pageContainer = isstring( element.tagName ) && /^(html|body)$/gi.test( element.tagName );
            
            switch ( dir ) {
                
                case 'scrollTop':
                    return element[ 'scrollTopMax' ] || element.scrollHeight - Math.min( element.clientHeight ,
                        element.offsetHeight , pageContainer ? global.innerHeight : Infinity );
                
                case 'scrollLeft':
                    return element[ 'scrollLeftMax' ] || element.scrollWidth - Math.min( element.clientWidth ,
                        element.offsetWidth , pageContainer ? global.innerWidth : Infinity );
                
            }
        }
        
        function scroll ( element , property , target , callback ) {
            var kill ,
                stopIncrement ,
                event = new eventHandler() ,
                $element = Kitten.picker( element ) ,
                max = scrollMax( element , property );
            
            if ( max <= 0 ) {
                return;
            }
            
            if ( target > max ) {
                target = max;
            }
            else if ( target < 0 ) {
                target = 0;
            }
            
            var currentValue = element[ property ] ,
                reverse = currentValue > target;
            
            if ( kill = $element.data( 'kitten.' + property ) ) {
                kill();
            }
            
            $element.data( 'kitten.' + property , kill = function () {
                $element.data( 'kitten.' + property , null );
                event.destroy();
                
                if ( isfunction( stopIncrement ) ) {
                    stopIncrement();
                    stopIncrement = null;
                }
                
                if ( isfunction( callback ) ) {
                    callback();
                    callback = null;
                }
            } );
            
            stopIncrement = smoothyIncrement( {
                begin : currentValue ,
                end : target ,
                onFinish : kill ,
                eachFrame : function ( totalvalue , increment ) {
                    
                    if ( reverse ) {
                        increment *= -1;
                    }
                    
                    element[ property ] += increment;
                    
                }
            } );
            
            event.listen( element , 'DOMMouseScroll , mousewheel' , kill , {
                passive : true ,
                once : true
            } );
        }
        
        return {
            
            scrollTop : function ( element , target , callback ) {
                scroll( element , 'scrollTop' , target , callback );
            } ,
            
            scrollLeft : function ( element , target , callback ) {
                scroll( element , 'scrollLeft' , target , callback );
            } ,
            
            scroll : function ( element , targetTop , targetLeft , callback ) {
                var caller = counterCallback( 2 , callback );
                scroll( element , 'scrollTop' , targetTop , caller );
                scroll( element , 'scrollLeft' , targetLeft , caller );
            }
            
        };
        
    } )();
    
    // ----- functions
    // ------------------------------------------------------------
    // ------------------------------------------------------------
    
    /**
     * @readonly
     * @function loop
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'loop' , function ( fn ) {
        if ( isfunction( fn ) ) {
            for ( var i = 0 , list = this , l = list.length , e ; i < l ; i++ ) {
                e = list[ i ];
                fn.call( e , e , i , list );
            }
        }
        
        return this;
    } );
    
    // css filters
    // ---------------------------
    
    /**
     * @readonly
     * @function is
     * @memberof KittenInstance
     * @return {!boolean}
     * */
    _defineProperty( Kitten.fn , 'is' , function ( filter ) {
        if ( this.length && isstring( filter ) ) {
            return target.match( this[ 0 ] , filter );
        }
        
        return false;
    } );
    
    /**
     * @readonly
     * @function hasin
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'hasin' , function ( filter ) {
        var result = [];
        
        if ( isstring( filter ) ) {
            this.loop( function ( element ) {
                if ( target.find( [ element ] , filter ).length ) {
                    result.push( element );
                }
            } );
        }
        
        return Kitten.factory( result );
    } );
    
    /**
     * @readonly
     * @function notin
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'notin' , function ( filter ) {
        var result = [];
        
        if ( isstring( filter ) ) {
            this.loop( function ( element ) {
                if ( !target.find( [ element ] , filter ).length ) {
                    result.push( element );
                }
            } );
        }
        
        return Kitten.factory( result );
    } );
    
    /**
     * @readonly
     * @function has
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'has' , function ( filter ) {
        var result = [];
        
        if ( isstring( filter ) ) {
            result = target.filter( clonearray( this ) , filter );
        }
        
        return Kitten.factory( result );
    } );
    
    /**
     * @readonly
     * @function not
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'not' , function ( filter ) {
        var result = [];
        
        if ( isstring( filter ) ) {
            this.loop( function ( element ) {
                if ( !target.match( element , filter , element.parentNode ) ) {
                    result.push( element );
                }
            } );
        }
        
        return Kitten.factory( result );
    } );
    
    // css browsers
    // ---------------------------
    
    /**
     * @readonly
     * @function find
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'find' , function ( selector ) {
        var result = [];
        
        if ( isstring( selector ) ) {
            result = target.find( clonearray( this ) , selector );
        }
        
        return Kitten.factory( result );
    } );
    
    /**
     * @readonly
     * @function merge
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'merge' , function ( selector ) {
        var current = clonearray( this );
        
        concat.apply( current , Kitten.picker( selector ) );
        
        return Kitten.factory( current );
    } );
    
    /**
     * @readonly
     * @function parent
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'parent' , function ( selector ) {
        var arg = ':parent';
        
        if ( isstring( selector ) ) {
            arg += '(' + selector + ')';
        }
        
        return Kitten.factory( target.apply( clonearray( this ) , arg ) );
    } );
    
    /**
     * @readonly
     * @function children
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'children' , function ( selector ) {
        var arg = ':children';
        
        if ( isstring( selector ) ) {
            arg += '(' + selector + ')';
        }
        
        return Kitten.factory( target.apply( clonearray( this ) , arg ) );
    } );
    
    /**
     * @readonly
     * @function closest
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'closest' , function ( selector ) {
        var arg = ':closest';
        
        if ( isstring( selector ) ) {
            arg += '(' + selector + ')';
        }
        
        return Kitten.factory( target.apply( clonearray( this ) , arg ) );
    } );
    
    // events handler
    // ---------------------------
    
    /**
     * @readonly
     * @function listen
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'listen' , function ( events , selector , callback , capture ) {
        var eventHandler = Kitten.eventHandler;
        
        return this.loop( function ( element ) {
            eventHandler.listen( element , events , selector , callback , capture );
        } );
    } );
    
    /**
     * @readonly
     * @function mute
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'mute' , function ( events , selector , callback , capture ) {
        var eventHandler = Kitten.eventHandler;
        
        return this.loop( function ( element ) {
            eventHandler.mute( element , events , selector , callback , capture );
        } );
    } );
    
    /**
     * @readonly
     * @function muteChild
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'muteChild' , function ( event , callback ) {
        var eventHandler = Kitten.eventHandler;
        
        return this.loop( function ( element ) {
            eventHandler.muteChild( element , event , callback );
        } );
    } );
    
    /**
     * @readonly
     * @function dispatch
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'dispatch' , function ( events , arg , forceEmulate ) {
        var eventHandler = Kitten.eventHandler;
        
        return this.loop( function ( element ) {
            eventHandler.dispatch( element , events , arg , forceEmulate );
        } );
    } );
    
    /**
     * @readonly
     * @function localDispatch
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'localDispatch' , function ( events , data ) {
        var eventHandler = Kitten.eventHandler;
        
        return this.loop( function ( element ) {
            eventHandler.localDispatch( element , events , data );
        } );
    } );
    
    // events shortcuts
    eventHandler.shortcuts.forEach( function ( name ) {
        _defineProperty( Kitten.fn , name , function ( selector , callback , capture ) {
            var eventHandler = Kitten.eventHandler;
            
            return this.loop( function ( element ) {
                eventHandler[ name ]( element , selector , callback , capture );
            } );
        } );
    } );
    
    // DOM utilities
    // ---------------------------
    
    /**
     * @readonly
     * @function clone
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'clone' , function ( children ) {
        var result = [];
        
        this.loop( function ( element ) {
            result.push( element.cloneNode( !!children ) );
        } );
        
        return Kitten.factory( result );
    } );
    
    /**
     * @readonly
     * @function cut
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'cut' , function () {
        return this.loop( function ( element ) {
            if ( exist( element.parentNode ) ) {
                element.parentNode.removeChild( element );
            }
        } );
    } );
    
    /**
     * @readonly
     * @function remove
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'remove' , function () {
        var eventHandler = Kitten.eventHandler ,
            animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            
            for ( var a = getallchildren( element ) , i = 0 , l = a.length , e ; i < l ; i++ ) {
                e = a[ i ];
                Kitten.eraseData( e );
                eventHandler.mute( e );
                animationHandler.kill( e );
            }
            
            if ( exist( element.parentNode ) ) {
                element.parentNode.removeChild( element );
            }
            
        } );
    } );
    
    /**
     * @readonly
     * @function append
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'append' , DomMutationAppend.factory( 'append' ) );
    
    /**
     * @readonly
     * @function prepend
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'prepend' , DomMutationAppend.factory( 'prepend' ) );
    
    /**
     * @readonly
     * @function appendBefore
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'appendBefore' , DomMutationAppend.factory( 'appendBefore' ) );
    
    /**
     * @readonly
     * @function appendAfter
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'appendAfter' , DomMutationAppend.factory( 'appendAfter' ) );
    
    /**
     * @readonly
     * @function appendTo
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'appendTo' , DomMutationMove.factory( 'append' ) );
    
    /**
     * @readonly
     * @function prependTo
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'prependTo' , DomMutationMove.factory( 'prepend' ) );
    
    /**
     * @readonly
     * @function appendBeforeTo
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'appendBeforeTo' , DomMutationMove.factory( 'appendBefore' ) );
    
    /**
     * @readonly
     * @function appendAfterTo
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'appendAfterTo' , DomMutationMove.factory( 'appendAfter' ) );
    
    /**
     * @readonly
     * @function wrapIn
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'wrapIn' , function ( str ) {
        
        var pattern = toarray( htmlDOM( str ) )[ 0 ];
        
        if ( !pattern ) {
            return this;
        }
        
        emptyNode( pattern );
        
        return this.loop( function ( element ) {
            
            var node = pattern.cloneNode( true );
            
            element.parentNode.replaceChild( node , element );
            
            node.appendChild( element );
            
        } );
        
    } );
    
    // content utilities
    // ---------------------------
    
    /**
     * @readonly
     * @function html
     * @memberof KittenInstance
     * @return {!(KittenInstance|string)}
     * */
    _defineProperty( Kitten.fn , 'html' , Content.factory( 'innerHTML' ) );
    
    /**
     * @readonly
     * @function text
     * @memberof KittenInstance
     * @return {!(KittenInstance|string)}
     * */
    _defineProperty( Kitten.fn , 'text' , Content.factory( 'textContent' ) );
    
    // class utilities
    // ---------------------------
    
    /**
     * @readonly
     * @function addClass
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'addClass' , ClassList.factory( 'add' , true ) );
    
    /**
     * @readonly
     * @function removeClass
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'removeClass' , ClassList.factory( 'remove' , true ) );
    
    /**
     * @readonly
     * @function toggleClass
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'toggleClass' , ClassList.factory( 'toggle' ) );
    
    /**
     * @readonly
     * @function hasClass
     * @memberof KittenInstance
     * @return {!boolean}
     * */
    _defineProperty( Kitten.fn , 'hasClass' , function ( value ) {
        var first , list;
        
        if ( isstring( value ) ) {
            value = splitTrim( value , ',' );
        }
        
        if ( ( first = this[ 0 ] ) && isarray( value ) ) {
            list = typedtoarray( first.classList );
            
            for ( var i = 0 , l = value.length ; i < l ; i++ ) {
                if ( inarray( value[ i ] , list ) ) {
                    return true;
                }
            }
        }
        
        return false;
    } );
    
    // input value
    // ---------------------------
    
    /**
     * @readonly
     * @function val
     * @memberof KittenInstance
     * @return {?(KittenInstance|string)}
     * */
    _defineProperty( Kitten.fn , 'val' , function ( value ) {
        
        if ( !isset( value ) && this.length ) {
            return this[ 0 ].value || null;
        }
        
        return this.loop( function () {
            this.value = value || null;
        } );
        
    } );
    
    // attributes utilities
    // ---------------------------
    
    /**
     * @readonly
     * @function attr
     * @memberof KittenInstance
     * @return {?(KittenInstance|string)}
     * */
    _defineProperty( Kitten.fn , 'attr' , KeyValue.factory( {
        
        get : function ( element , key ) {
            return element.getAttribute( key );
        } ,
        
        set : function ( element , key , value ) {
            if ( isnull( value ) ) {
                element.removeAttr( key );
            }
            else {
                element.setAttribute( key , value );
            }
        }
        
    } ) );
    
    /**
     * @readonly
     * @function removeAttr
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'removeAttr' , function ( name ) {
        
        if ( isstring( name ) ) {
            name = splitTrim( name , ',' );
        }
        
        if ( isarray( name ) ) {
            return this.loop( function ( element ) {
                for ( var i = 0 , l = name.length ; i < l ; i++ ) {
                    element.removeAttribute( name[ i ] );
                }
            } );
            
        }
        
        return this;
    } );
    
    // css properties
    // ---------------------------
    
    /**
     * @readonly
     * @function css
     * @memberof KittenInstance
     * @return {?(KittenInstance|string)}
     * */
    _defineProperty( Kitten.fn , 'css' , KeyValue.factory( {
        
        get : function ( element , property ) {
            return element.style.getPropertyValue( property ) || getstyleproperty( element , property );
        } ,
        
        set : function ( element , property , value ) {
            if ( value ) {
                element.style.setAttribute( property , value );
            }
            else {
                element.style.removeProperty( property );
            }
        }
        
    } ) );
    
    // animations handler
    // ---------------------------
    
    /**
     * @readonly
     * @function animate
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'animate' , function ( css , duration , callback , merge ) {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.animate( element , css , duration , callback , merge );
        } );
    } );
    /**
     * @readonly
     * @function wait
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'wait' , function ( duration , callback ) {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.wait( element , duration , callback );
        } );
    } );
    
    /**
     * @readonly
     * @function fadeIn
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'fadeIn' , function ( duration , callback ) {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.fadeIn( element , duration , callback );
        } );
    } );
    
    /**
     * @readonly
     * @function fadeOut
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'fadeOut' , function ( duration , callback ) {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.fadeOut( element , duration , callback );
        } );
    } );
    
    /**
     * @readonly
     * @function toggleFade
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'toggleFade' , function ( duration , callback ) {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.toggleFade( element , duration , callback );
        } );
    } );
    
    /**
     * @readonly
     * @function slideDown
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'slideDown' , function ( duration , callback ) {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.slideDown( element , duration , callback );
        } );
    } );
    
    /**
     * @readonly
     * @function slideUp
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'slideUp' , function ( duration , callback ) {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.slideUp( element , duration , callback );
        } );
    } );
    
    /**
     * @readonly
     * @function slideLeft
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'slideLeft' , function ( duration , callback ) {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.slideLeft( element , duration , callback );
        } );
    } );
    
    /**
     * @readonly
     * @function slideRight
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'slideRight' , function ( duration , callback ) {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.slideRight( element , duration , callback );
        } );
    } );
    
    /**
     * @readonly
     * @function toggleSlide
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'toggleSlide' , function ( duration , callback ) {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.toggleSlide( element , duration , callback );
        } );
    } );
    
    /**
     * @readonly
     * @function turnIn
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'turnIn' , function ( duration , callback ) {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.turnIn( element , duration , callback );
        } );
    } );
    
    /**
     * @readonly
     * @function turnOut
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'turnOut' , function ( duration , callback ) {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.turnOut( element , duration , callback );
        } );
    } );
    
    /**
     * @readonly
     * @function toggleTurn
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'toggleTurn' , function ( duration , callback ) {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.toggleTurn( element , duration , callback );
        } );
    } );
    
    /**
     * @readonly
     * @function plopIn
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'plopIn' , function ( duration , callback ) {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.plopIn( element , duration , callback );
        } );
    } );
    
    /**
     * @readonly
     * @function plopOut
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'plopOut' , function ( duration , callback ) {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.plopOut( element , duration , callback );
        } );
    } );
    
    /**
     * @readonly
     * @function togglePlop
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'togglePlop' , function ( duration , callback ) {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.togglePlop( element , duration , callback );
        } );
    } );
    
    /**
     * @readonly
     * @function show
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'show' , function () {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.show( element );
        } );
    } );
    
    /**
     * @readonly
     * @function hide
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'hide' , function () {
        var animationHandler = Kitten.animationHandler;
        
        return this.loop( function ( element ) {
            animationHandler.hide( element );
        } );
    } );
    
    // storage
    // ---------------------------
    
    ( function () {
        
        var id = 0 ,
            data = {} ,
            storageKey = randomstring() + 'KittenData';
        
        function getData ( element ) {
            var i , o;
            
            if ( !( i = element[ storageKey ] ) ) {
                element[ storageKey ] = i = ( ++id );
            }
            
            if ( !( o = data[ i ] ) ) {
                data[ i ] = o = {};
            }
            
            return o;
        }
        
        
        /**
         * @private
         * @readonly
         * @function eraseData
         * @param {HTMLElement|Element|Node} element
         * @memberof Kitten
         * @return void
         * */
        _defineProperty( Kitten , 'eraseData' , function ( element ) {
            var tmp , kill ,
                $element = Kitten.picker( element );
            
            if ( isfunction( kill = $element.data( 'kitten.scrollto' ) ) ) {
                kill();
            }
            
            if ( tmp = element[ storageKey ] ) {
                delete element[ storageKey ];
                delete data[ tmp ];
            }
        } );
        
        /**
         * @readonly
         * @function data
         * @memberof KittenInstance
         * @return {?*}
         * */
        _defineProperty( Kitten.fn , 'data' , KeyValue.factory( {
            
            get : function ( element , key ) {
                var tmp = getData( element );
                
                if ( hasProperty.call( tmp , key ) ) {
                    return tmp[ key ] || null;
                }
                
                return null;
            } ,
            
            set : function ( element , key , value ) {
                var data = getData( element );
                
                if ( isnull( value ) ) {
                    delete data[ key ];
                }
                else {
                    data[ key ] = value;
                }
            }
            
        } ) );
        
    } )();
    
    // scroll
    // ---------------------------
    
    /**
     * @readonly
     * @function scrollTop
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'scrollTop' , function ( target , callback ) {
        return this.loop( function ( element ) {
            if ( exist( element.scrollTop ) ) {
                ScrollAPI.scrollTop( element , target , callback );
            }
        } );
    } );
    
    /**
     * @readonly
     * @function scrollLeft
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'scrollLeft' , function ( target , callback ) {
        return this.loop( function ( element ) {
            if ( exist( element.scrollLeft ) ) {
                ScrollAPI.scrollLeft( element , target , callback );
            }
        } );
    } );
    
    /**
     * @readonly
     * @function scrollTo
     * @memberof KittenInstance
     * @return {KittenInstance}
     * */
    _defineProperty( Kitten.fn , 'scrollTo' , function ( top , left , callback ) {
        return this.loop( function ( element ) {
            if ( exist( element.scrollTop ) && exist( element.scrollLeft ) ) {
                ScrollAPI.scroll( element , top , left , callback );
            }
        } );
    } );
    
    // $
    // ---------------------------
    ( function ( old ) {
        
        /**
         * @function $
         * @return {KittenInstance}
         * */
        global.$ = Kitten.picker;
        
        /**
         * @property {Kitten.fn} $.fn
         * */
        $.fn = Kitten.fn;
        
        /**
         * @property {domLoad} $.ready
         * */
        $.ready = domLoad;
        
        /**
         * @property {window.ajax} $.ajax
         * */
        $.ajax = ajax;
        
        /**
         * @property {window.getText} $.getText
         * */
        $.getText = getText;
        
        /**
         * @property {window.getJson} $.getJson
         * */
        $.getJson = getJson;
        
        /**
         * @property {window.getXML} $.getXML
         * */
        $.getXML = getXML;
        
        /**
         * @property {window.getHTML} $.getHTML
         * */
        $.getHTML = getHTML;
        
        /**
         * @property {window.getFile} $.getFile
         * */
        $.getFile = getFile;
        
        /**
         * @public
         * @readonly
         * @function kitten
         * @return KittenInstance
         * */
        _defineProperty( global , 'Kitten' , Kitten );
        
        exist( old ) && ( global.$.old = old );
        
    } )( global.$ );
    
    console.timeEnd( 'Kitten build time' );
    
} ) ) );